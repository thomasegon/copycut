#ifndef XML_H
#define XML_H
#include <string>
#include <iostream>
#include <opencv2/core/core.hpp>

bool XML_file_exists();

void Write_Settings_To_XML(int turdSize, int alphaMax, int tolerance, bool blurring, int morph_iterations,
	double binary_thres, double scale, bool optimize, bool invert, int roi1x, int roi1y, int roi2x, int roi2y);




#endif