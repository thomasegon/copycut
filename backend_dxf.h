#ifndef BACKEND_DXF_H
#define BACKEND_DXF_H
extern "C"
{
#include "lib_windows\main.h"
}

// dpoint_t sub(dpoint_t v, dpoint_t w);
// double iprod(dpoint_t v, dpoint_t w);
// double xprod(dpoint_t v, dpoint_t w);
// double bulge(dpoint_t v, dpoint_t w);

int page_header(std::ofstream &myfile, double entmax_x, double entmax_y);
int page_polyline(std::ofstream &myfile);
int page_vertex(std::ofstream &myfile, int layer, dpoint_t v, double bulge);
int page_seqend(std::ofstream &myfile);
int page_end(std::ofstream &myfile);
int bezier(std::ofstream &myfile, int layer, dpoint_t A, dpoint_t B, dpoint_t C, dpoint_t D);
dpoint_t scalePoint(dpoint_t point, double scale);

#endif // BACKEND_DXF_H