#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace cv;
using namespace std;






int main(){
    Mat image = imread("glare2.jpg", CV_LOAD_IMAGE_COLOR);
    //GaussianBlur(image, image, Size(111,111), 3.0 );

    
    // READ RGB color image and convert it to Lab
    cv::Mat bgr_image = image;
    cv::Mat lab_image;
    cv::cvtColor(bgr_image, lab_image, CV_BGR2Lab);

    // Extract the L channel
    std::vector<cv::Mat> lab_planes(3);
    cv::split(lab_image, lab_planes);  // now we have the L image in lab_planes[0]

    // apply the CLAHE algorithm to the L channel
    cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
    clahe->setClipLimit(4);
    cv::Mat dst;
    clahe->apply(lab_planes[0], dst);

    // Merge the the color planes back into an Lab image
    dst.copyTo(lab_planes[0]);
    cv::merge(lab_planes, lab_image);

   // convert back to RGB
   cv::Mat image_clahe;
   cv::cvtColor(lab_image, image_clahe, CV_Lab2BGR);

   // display the results  (you might also want to see lab_planes[0] before and after).
    cv::imshow("image original", bgr_image);
    namedWindow( "clahe", WINDOW_NORMAL );
    resizeWindow("clahe", 1000,1000);
    imshow("clahe", image_clahe);
    waitKey(0);
    cv::waitKey();

}