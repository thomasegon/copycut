#ifndef BACKEND_IMG_H
#define BACKEND_IMG_H
#include <iostream>
#include <fstream>
#include <string>
#include "HTML.h"
#include <msclr\marshal_cppstd.h>
extern "C"
{
#include "lib_windows\main.h"
}

/// <summary> Creates a html string for showing the image</summary>
/// <param name="image"> The images to view in browser </param>
/// <param name="worker"> The background worker running the process. Used to cancel the function before completion if cancellationPending is true</param>
/// <param name="e"> Used to set the cancel flag </param>
/// <returns> string containing the html for the browser </returns>
std::string makeImg(System::Drawing::Bitmap^ image);


#endif // BACKEND_IMG_H