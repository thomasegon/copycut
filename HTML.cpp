#include "HTML.h"
using namespace std;
string workingdir()
{
	char buf[256];
	GetCurrentDirectoryA(256, buf);
	return string(buf) + '\\';
}
string startHTML() {
	string html;
	html = html + "<!DOCTYPE html>" + "\n" "\n"
		+ "<html>" + "\n"
		+ "<head>" + "\n"
		+ "  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=9\"/>" + "\n" // We need to specify that it is internet explore 9
		+ "  <script src=\"" + workingdir() + "js\\jquery.js\"></script>" + "\n" //Not sure that it works when the code is compiled?
		+ "  <script src=\"" + workingdir() + "js\\jquery.panzoom.js\"></script>" + "\n"
		+ "  <script src=\"" + workingdir() + "js\\jquery.mousewheel.js\"></script>" + "\n"
		+ "<style type = \"text/css\">" + "\n"
		+ "button{"
		+ "background-color: #49617d;"
		+ "border: solid;"
		+ "border-color: black;"
		+ "border-width: 2px;"
		+ "color: white;"
		+ "padding: 5px 5px;"
		+ "text-align: center;"
		+ "text-decoration: none;"
		+ "display: inline - block;"
		+ "font-size: 12px;"
		+ "}"
		+ "</style>"
		+ "</head>" + "\n"
		+ "<body>" + "\n"
		+ "<div class=\"parent\">" + "\n"
		+ "<div style='width:100%; height: 86vh; text-align: center;' class=\"panzoom\">" + "\n";
	return html;
}
string endHTML() {
	string html;
	html = html +
		+"</div>" + "\n"
		+ "<div class='buttons' style='margin:5px; z-index:1000; position:relative; top:0px left:0px;'>" + "\n"
		+ "<button class='zoom-in'>Zoom In</button>" + "\n"
		+ "<button class='zoom-out'>Zoom Out</button>" + "\n"
		+ "<button class='reset'>Reset</button>" + "\n"
		+ "</div>" + "\n"
		+ "</div>" + "\n"
		+ "<script>" + "\n"
		+ "(function() {" + "\n"
		+ "var $panzoom = $('.panzoom').panzoom({" + "\n"
		+ "$zoomIn: $('.zoom-in')," + "\n"
		+ "$zoomOut : $('.zoom-out')," + "\n"
		+ "$zoomRange : $('.zoom-range')," + "\n"
		+ "$reset : $('.reset')," + "\n"
		+ "minScale: 0.5," + "\n"
		+ "maxScale: 200," + "\n"
		//+ "contain: 'automatic'" + "\n"
		+ "});" + "\n"
		+ "$panzoom.on('dblclick', function(e){" + "\n"
		+ "e.preventDefault();" + "\n"
		+ "$panzoom.panzoom('zoom', {focal: e})" + "\n"
		+ "});" + "\n"
		+ "$panzoom.parent().on('mousewheel.focal', function(e) {" + "\n"
		+ "e.preventDefault();" + "\n"
		+ "var delta = e.delta || e.originalEvent.wheelDelta;" + "\n"
		+ "var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;" + "\n"
		+ "$panzoom.panzoom('zoom', zoomOut, {" + "\n"
		+ "animate: false," + "\n"
		+ "focal : e" + "\n"
		+ "});" + "\n"
		+ "});" + "\n"
		+ "})();" + "\n"
		+ "</script>" + "\n"
		+ "</body>" + "\n"
		+ "</html>" + "\n";
	return html;
}
string createHTML(string html) {
	string ret = startHTML() + html + endHTML();
	return ret;
}