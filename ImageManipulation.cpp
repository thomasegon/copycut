#include "ImageManipulation.h"
#include "windows.h"
#include <experimental/filesystem>
#include "PolylineUnion.h"


using namespace cv;
using namespace std;

struct InputParam
{
	Mat img;
	Mat orgImg;
	string imgName;
	Point pt;
	Point prevPt;
	vector<Point> result;
};

/// <summary> Blurring smooths out the image by using morphologyEx from Opencv </summary>
/// <param name="src"> The input/output image to be blurred </param>
/// <param name="iterations"> How many blurring iterations to do. More iterations will smooth out the image more, removing jagged lines, but also rounding corners</param>
/// <param name="worker"> The background worker running the process. Used to for cancellation before the function is finished </param>
/// <param name="e"> DoWorkEventArgs used to set the cancel flag </param>
/// <returns> Void </returns>
void Blurring(Mat *src, int iterations, System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e){
    int morph_elem = 0;
    int morph_size = 0;
    for(int i = 1; i<iterations; i+=2){
		if (worker->CancellationPending)
		{
			worker->ReportProgress(1); // tell progress it process is canceled
			e->Cancel = true;
			return;
		}
        Mat element = getStructuringElement( MORPH_ELLIPSE, Size(i+1, i+1 ), Point(-1, -1) );
        morphologyEx( *src, *src, MORPH_OPEN, element); // Har potentiale til at være et problem, mangler defualt parametere
        morphologyEx( *src, *src, MORPH_CLOSE, element);// Samme her
    }
    
    Mat element = getStructuringElement( MORPH_ELLIPSE, Size(3, 3), Point(-1, -1) );
    morphologyEx( *src, *src, MORPH_ERODE, element);

    return;
}


void Erotion(Mat *src, int offset) {
	// Erodes the contours
	// Offset is the number of pizels added to both sides of the contour
	// Writes the new image into the reference src
	erode(*src, *src, Mat(), Point(-1, -1), offset);
}

void Dilation(Mat *src, int offset) {
	// Dilates the contours
	// Offset is the number of pizels added to both sides of the contour
	// Writes the new image into the reference src
	dilate(*src, *src, Mat(), Point(-1, -1), offset);
}

/// <summary> Otsu is a "smart" binary threshold, which will attempt to find the threshold automatically </summary>
/// <param name="src"> The input/output image to be blurred </param>
/// <param name="thres"> The initial threshold parameter, but will otherwise not affect the result </param>
/// <param name="maxvalue"> The max pixel value. Should always be 255. Lower values will only make the image lighter </param>
/// <returns> Void </returns>
void Otsu(Mat *src, int thresh, double maxvalue){
    vector<Mat> rgbChannels(3);
    split(*src, rgbChannels);
    for (size_t i = 0; i < 3; i++)
    {
        threshold(rgbChannels[i], rgbChannels[i], thresh, maxvalue, THRESH_OTSU);   
    }

    merge(rgbChannels, *src);
    return;
}

/// <summary> Binary threshold, which is used to make a gray image into a binary image with only black and white for better contrast </summary>
/// <param name="src"> The input/output image to be blurred </param>
/// <param name="value"> The treshold value, which spceify at what pixel value a pixel will be consider either black or white </param>
/// <param name="maxvalue"> The max pixel value. Should always be 255. Lower values will only make the image lighter </param>
/// <returns> Void </returns>
void Binary(Mat *src, double value, double maxvalue){
    Mat grayImage;
	if (src->channels() > 1) {
		cvtColor(*src, grayImage, CV_BGR2GRAY);
	}
	else
		grayImage = src->clone();
    threshold(grayImage,*src, value, maxvalue, THRESH_BINARY);
    return;
}



void Clahe(Mat *src){

    Mat lab_image;
    cvtColor(*src, lab_image, CV_BGR2Lab);
    // Extract the L channel
    vector<Mat> lab_planes(3);
    split(lab_image, lab_planes);

    // apply the CLAHE algorithm to the L channel
    Ptr<CLAHE> clahe = createCLAHE();
    clahe->setClipLimit(4);
    Mat dst;
    clahe->apply(lab_planes[0], dst);

    // Merge the the color planes back into an Lab image
    dst.copyTo(lab_planes[0]);
    merge(lab_planes, lab_image);

    // convert back to RGB
    cvtColor(lab_image, *src, CV_Lab2BGR);
}


Mat BackgroundSubtraction(bool debug)
{

    //Ptr<BackgroundSubtractor> bg_model = createBackgroundSubtractorMOG2(1, 16.0, true);
	std::cout << "Press any key to take background picture \n";
	getchar();
	
	Mat background = getMatFromStream(debug);

	std::cout << "Press any key to take foreground picture \n";
	getchar();
	Mat foreground = getMatFromStream(debug);
    Mat foregroundMask;

    // bg_model->apply(background, foregroundMask);
    // bg_model->apply(foreground, foregroundMask);
    absdiff(foreground, background, foregroundMask);

    return foregroundMask;
}


void Showim(Mat image, bool resize, string name, double factor){
	
	namedWindow(name, WINDOW_NORMAL);

	if (resize) {
		RECT desktop;
		const HWND hDestop = GetDesktopWindow();
		GetWindowRect(hDestop, &desktop);

		if (image.cols > image.rows) {
			double scalefactor = ((double)image.cols / ((double)desktop.right / factor));
			resizeWindow(name, image.cols / scalefactor, image.rows / scalefactor);
		}
		else {
			double scalefactor = ((double)image.rows / ((double)desktop.bottom / factor));
			resizeWindow(name, image.cols / scalefactor, image.rows / scalefactor);
		}

	}

	imshow(name, image);

	waitKey(0);
	destroyAllWindows();
}


void Showim(vector<Mat> images, bool resize, string name, double factor){
	for (size_t i = 0; i < images.size(); i++)
	{
		string loopname = name + to_string(i);
		namedWindow(loopname, WINDOW_NORMAL);
		if (resize) {
			RECT desktop;
			const HWND hDestop = GetDesktopWindow();
			GetWindowRect(hDestop, &desktop);

			if (images[i].cols > images[i].rows) {
				double scalefactor = ((double)images[i].cols / (double)(desktop.right / factor));
				resizeWindow(loopname, images[i].cols / scalefactor, images[i].rows / scalefactor);
			}
			else {
				double scalefactor = ((double)images[i].rows / ((double)desktop.bottom / factor));
				resizeWindow(loopname, images[i].cols / scalefactor, images[i].rows / scalefactor);
			}
			
		}
		
		imshow(loopname, images[i]);
	}

	waitKey(0);
	destroyAllWindows();
}

void FindROI(Mat *src, int point1_x, int point1_y, int point2_x, int point2_y) {
	Rect ROI;

	ROI.x = point1_x;
	ROI.y = point1_y;
	ROI.width = point2_x - point1_x;
	ROI.height = point2_y - point1_y;

	*src = (*src)(ROI);
}
void FindROI(Mat *src, vector<Point> points) {
	Rect ROI;

	ROI.x = points[0].x;
	ROI.y = points[0].y;
	ROI.width = points[1].x - points[0].x;
	ROI.height = points[1].y - points[0].y;

	*src = (*src)(ROI);
}

std::vector<std::string> get_filenames(std::experimental::filesystem::path path)
{
	namespace stdfs = std::experimental::filesystem;

	std::vector<std::string> filenames;

	// http://en.cppreference.com/w/cpp/experimental/fs/directory_iterator
	const stdfs::directory_iterator end{};

	for (stdfs::directory_iterator iter{ path }; iter != end; ++iter)
	{
		// http://en.cppreference.com/w/cpp/experimental/fs/is_regular_file 
		if (stdfs::is_regular_file(*iter)) // comment out if all names (names of directories tc.) are required
			filenames.push_back(iter->path().string());
	}

	return filenames;
}
template <typename T>
vector<vector<T>> extract_points(vector<vector<T>> original, int increment) {
	vector<vector<T>> new_vector;
	for (int i = 0; i < original.size(); i += increment) {
		new_vector.push_back(original[i]);
	}
	return new_vector;
}



vector<vector<Point>> FindApprox(vector<vector<Point>> contours, double epsilon){
	vector<vector<Point> > foundContours;
	foundContours.resize(contours.size());
	for (size_t k = 0; k < contours.size(); k++) {
		//epsilon = epsilon * arcLength(contours[k], true);
		approxPolyDP(Mat(contours[k]), foundContours[k], epsilon, true);
	}
	return foundContours;
}


vector<vector<Point> > FindPelicanApprox(Mat image, double epsilon, bool debugging) {
	vector<vector<Point> > contours;
	findContours(image, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE); // CV_RETR_EXTERNAL gives outer contours, so if we have one contour enclosing another only the outermost is given.
	vector<vector<Point> > approx = FindApprox(contours, epsilon);
	vector<vector<Point>> result = PolyUnion(approx, contours);
	if (debugging) {
		Mat hvid = Mat(Size
		(3456, 5184), CV_8UC3);
		drawContours(hvid, approx, -1, Scalar(255, 255, 255), 2);
		drawContours(hvid, contours, -1, Scalar(0, 255, 0));
		drawContours(hvid, result, -1, Scalar(0, 0, 255), 2);
		cout << "Writing" << endl;
		imwrite("CheckPelicanApprox.bmp", hvid);
	}
	
	//Insert union from boost library
	return result;
}

Mat aproxPoly(Mat image, double epsilon) {
	vector<vector<Point> > contours;
	findContours(image, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE); // CV_RETR_EXTERNAL gives outer contours, so if we have one contour enclosing another only the outermost is given.
	vector<vector<Point>> approx = FindApprox(contours, epsilon);

	Mat new_image = Mat::zeros(Size(image.cols, image.rows), CV_8UC3);
	drawContours(new_image, approx, -1, Scalar(255, 255, 255), -1);
	return new_image;
}

//Simple function to invert images.
void invertImage(Mat *image)
{
	subtract(Scalar::all(255), *image, *image);
	return;
}

static void onMouse(int event, int x, int y, int flags, void*returnResult)
{
	InputParam *param = (InputParam*)returnResult;
	Mat img = param->img;
	Mat orgImg = param->orgImg;
	string imgName = param->imgName;
	if (x < 0 || x >= img.cols || y < 0 || y >= img.rows)
		return;
	if (event == EVENT_LBUTTONUP || !(flags & EVENT_FLAG_LBUTTON)) {
		param->prevPt = Point(-1, -1);
	}
	// If we want to use the shift button for something then here is an example of how it can be implemented.
	//else if (flags == 17 && event == EVENT_LBUTTONDOWN)
	//	cout << "Shift" << endl;
	else if (event == EVENT_LBUTTONDOWN)
		param->prevPt = Point(x, y);
	else if (event == EVENT_MOUSEMOVE && (flags & EVENT_FLAG_LBUTTON))
	{
		param->pt = Point(x, y);
		if (param->prevPt.x < 0)
			param->prevPt = Point(x, y);
		rectangle(img, param->prevPt, param->pt, Scalar(0, 0, 255), 12);
		imshow(imgName, img);
		orgImg.copyTo(img);
		//Result contains two points the upper left point and the lower right point. The points are found by taking the smallest x and y for the first point and the largest for the second point
		param->result = vector<Point>{ 
			Point(param->prevPt.x < param->pt.x ? param->prevPt.x : param->pt.x, param->prevPt.y < param->pt.y ? param->prevPt.y : param->pt.y), 
			Point(param->prevPt.x > param->pt.x ? param->prevPt.x : param->pt.x, param->prevPt.y > param->pt.y ? param->prevPt.y : param->pt.y)};

		//line(img, prevPt, pt, Scalar(255, 0, 0), 5, 8, 0);
		//prevPt = pt;
	}
}
vector<Point> drawOnImage(Mat image, string imageName, bool resize, double factor) {
	namedWindow(imageName, WINDOW_NORMAL);
	if (resize) {
		RECT desktop;
		const HWND hDestop = GetDesktopWindow();
		GetWindowRect(hDestop, &desktop);

		if ( image.cols > image.rows) {
			double scalefactor = ((double)image.cols / ((double)desktop.right / factor));
			resizeWindow(imageName, image.cols / scalefactor, image.rows / scalefactor);
		}
		else {
			double scalefactor = ((double)image.rows / ((double)desktop.bottom / factor));
			resizeWindow(imageName, image.cols / scalefactor, image.rows / scalefactor);
		}

	}
	vector<Point> result;
	Mat img, orgImg;
	namedWindow(imageName, 1);
	image.copyTo(img);
	image.copyTo(orgImg);
	imshow(imageName, img);
	//InputParam is used instead of having global variables for the images and the points.
	InputParam param;
	param.img = img;
	param.orgImg = orgImg;
	param.imgName = imageName;
	param.pt = Point(-1, -1);
	param.prevPt = Point(-1, -1);

	setMouseCallback(imageName, onMouse, &param);
	waitKey(0);
	destroyAllWindows();


	// make sure all points are divisible by 4 for when we make bitmaps
	if (param.result.size() != 0) {
		Point p1 = param.result[0];
		Point p2 = param.result[1];
		int valuex = param.result[0].x - (param.result[0].x % 4);
		valuex = valuex > 0 ? valuex : 0;

		int valuey = param.result[0].y - (param.result[0].y % 4);
		valuey = valuey > 0 ? valuey : 0;
		param.result[0] = Point(valuex, valuey);

		valuex = param.result[1].x - (param.result[1].x % 4);
		valuex = valuex > 0 ? valuex : 0;

		valuey = param.result[1].y - (param.result[1].y % 4);
		valuey = valuey > 0 ? valuey : 0;
		param.result[1] = Point(valuex, valuey);
	}

	return param.result;
}


void ShadowRemoval(Mat *img, int blurIterations, int dilateKernelSize) {

	//code from https://stackoverflow.com/a/44752405/7736844
	
	vector<Mat> rgb_planes(3);
	split(*img, rgb_planes);

	Mat Result_Norm;


	vector<Mat> result_norm_planes;

	for (size_t i = 0; i < rgb_planes.size(); i++)
	{
		Mat temporary_image;
		dilate(rgb_planes[i], temporary_image, Mat::ones(dilateKernelSize, dilateKernelSize, CV_32F));

		medianBlur(temporary_image, temporary_image, blurIterations);

		absdiff(rgb_planes[i], temporary_image, temporary_image);
		subtract(Scalar::all(255), temporary_image, temporary_image);
		normalize(temporary_image, temporary_image, 0, 255, NORM_MINMAX, CV_8UC1);
		result_norm_planes.push_back(temporary_image);
	}
	
	merge(result_norm_planes, *img);

}

void walktree(vector<vector<Point>> *result, vector<vector<Point>> contours, int node, vector<Vec4i> hierarchy) {
	result->push_back(contours[node]);
	
	int next = 0;
	int child = 2;
	
	if (hierarchy[node][child] != -1) {
		walktree(result, contours, hierarchy[node][child], hierarchy);
	}

	if (hierarchy[node][next] != -1) {
		walktree(result, contours, hierarchy[node][next], hierarchy);
	}

	
	
}


vector<vector<Point>> FindLargestContour(vector<vector<Point>> contours, vector<Vec4i> hierarchy, int numberOfItems) {
	// Set the first Contour to be the largest contour
	vector<vector<Point>> largestContours;
	vector<int> largestContourIndex;
	bool firstIndex = true;
	// Iterates over all contours except the first one
	for (size_t i = 0; i < numberOfItems; i++)
	{
		int largestIndex = 0;
		double largestSize = 0;
		for (size_t j = 0; j < contours.size(); j++)
		{
			if (hierarchy[j][3] == -1 && find(largestContourIndex.begin(),largestContourIndex.end(), j) == largestContourIndex.end()) {
				double currentContourArea = contourArea(contours[j]);
				if (largestSize < currentContourArea) {
					largestIndex = j;
					largestSize = currentContourArea;
				}
			}
			
		}
		if (firstIndex || largestIndex != 0) {
			largestContours.push_back(contours[largestIndex]);
			largestContourIndex.push_back(largestIndex);

			if (hierarchy[largestIndex][2] != -1) {
				walktree(&largestContours, contours, hierarchy[largestIndex][2], hierarchy);
			}

			firstIndex = false;
		}
	}

	return largestContours;
}


void DeleteSmallContours(Mat *Image, int numberOfItems) {

	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	//Mat grayImage;
	//cv::cvtColor(*Image, grayImage, CV_RGB2GRAY);
	Mat newImage(Image->rows, Image->cols, Image->type(), Scalar(0, 0, 0));
	findContours(*Image, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE);

	drawContours(newImage, FindLargestContour(contours, hierarchy, numberOfItems), -1, Scalar(255, 255, 255), -1);

	newImage.copyTo(*Image);
	//*Image = newImage.clone();
}

double FindPaperLongSide(Mat image, double binary) {
	// returns the euclidian distance of the minimum area rectanlge of the outher contour
	// Assumes the outer contour is a piece of paper. 
	// returns the euclidian distance, of the longest side of the paper, in pixels

	std::vector<std::vector<cv::Point> > contours;
	vector<Vec4i> hierarchy;
	if (image.channels() > 1) {
		cvtColor(image, image, COLOR_BGR2GRAY);
	}

	Binary(&image, binary, 255);

	findContours(image, contours, hierarchy, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

	double largestDistance = 0;

	vector<Point> largestContour = FindLargestContour(contours, hierarchy, 1)[0];

	for (size_t i = 0; i < largestContour.size(); i++)
	{
		for (size_t j = 0; j < largestContour.size(); j++)
		{
			double tempLength = norm(largestContour[i] - largestContour[j]);
			if (tempLength > largestDistance) {
				largestDistance = tempLength;
			}
		}
	}	

	return largestDistance / 363.7;
}


// int main(){
//     Mat image;
//     image = imread("real_scrap.jpg", CV_LOAD_IMAGE_COLOR);//Input file
//     Mat image_bmp;
//     Showim(&image, 1000);
//     Blurring(&image, 2);
//     Showim(&image, 1000);
//     Otsu(&image);
//     Showim(&image, 1000);
//     Binary(&image, 240.0, 255.0);
//     Showim(&image, 1000);
    
    

//     image.convertTo(image_bmp, CV_8UC3);
//     imwrite("out.bmp", image_bmp);  
// }