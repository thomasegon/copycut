#include <iostream>
#include <fstream>
#include <math.h>
#include "backend_dxf.h"

using namespace std;

dpoint_t sub(dpoint_t v, dpoint_t w){
    dpoint_t r;

    r.x = v.x - w.x;
    r.y = v.y - w.y;
    return r;
}
double iprod(dpoint_t v, dpoint_t w){
    return v.x * w.x + v.y * w.y;
}
double xprod(dpoint_t v, dpoint_t w){
    return v.x * w.y - v.y * w.x;
}
double bulge(dpoint_t v, dpoint_t w){
    double v2, w2, vw, vxw, nvw;

    v2 = iprod(v, v);
    w2 = iprod(w, w);
    vw = iprod(v, w);
    vxw = xprod(v, w);
    nvw = sqrt(v2 * w2);

    if (vxw == 0.0) {
        return 0.0;
    }    
    return (nvw - vw) / vxw;
}
int page_header(ofstream &myfile, double entmax_x, double entmax_y){
    myfile << "999\n";
    myfile << "Thomas er den sejeste\n";
    myfile << "0\n";
    myfile << "SECTION\n";
    myfile << "2\n";
    myfile << "HEADER\n";
    myfile << "9\n";
    myfile << "$ACADVER\n";
    myfile << "1\n";
    myfile << "AC1006\n";
    myfile << "9\n";
    myfile << "$EXTMIN\n";
    myfile << "10\n";
    myfile << "0.0\n";
    myfile << "20\n";
    myfile << "0.0\n";
    myfile << "30\n";
    myfile << "0.0\n";
    myfile << "9\n";
    myfile << "$EXTMAX\n";
    myfile << "10\n";
    myfile << entmax_x <<"\n";
    myfile << "20\n";
    myfile << entmax_y <<"\n";
    myfile << "30\n";
    myfile << "0.0\n";
    myfile << "0\n";
    myfile << "ENDSEC\n";
    myfile << "0\n";
    myfile << "SECTION\n";
    myfile << "2\n";
    myfile << "ENTITIES\n";
    return 1;
}
int page_polyline(ofstream &myfile){
    myfile << "0\n";
    myfile << "POLYLINE\n";
    myfile << "8\n";
    myfile << "0\n";//Layer 0
    myfile << "66\n";
    myfile << "1\n";
    myfile << "70\n";
    myfile << "1\n";
    return 1;
}
int page_vertex(ofstream &myfile, int layer, dpoint_t v, double bulge){
    myfile << "0\n";
    myfile << "VERTEX\n";
    myfile << "8\n";
    myfile << layer <<"\n";
    myfile << "10\n";
    myfile << v.x << "\n";
    myfile << "20\n";
    myfile << v.y << "\n";
    myfile << "42\n";
    myfile << bulge << "\n";
    return 1;
}
int page_seqend(ofstream &myfile){
    myfile << "0\n";
    myfile << "SEQEND\n";
    return 1;
}
int page_end(ofstream &myfile){
    myfile << "0\n";
    myfile << "ENDSEC\n";
    myfile << "0\n";
    myfile << "EOF\n";
    return 1;
}
int pseudo_quad(ofstream &myfile, int layer, dpoint_t A, dpoint_t C, dpoint_t B){
    dpoint_t v, w;
    double v2, w2, vw, vxw, nvw;
    double a, b, c, y;
    dpoint_t G;
    double bulge1, bulge2;

    v = sub(A, C);
    w = sub(B, C);

    v2 = iprod(v, v);
    w2 = iprod(w, w);
    vw = iprod(v, w);
    vxw = xprod(v, w);
    nvw = sqrt(v2 * w2);

    a = v2 + 2*vw + w2;
    b = v2 + 2*nvw + w2;
    c = 4*nvw;
    if (vxw == 0 || a == 0) {
        page_vertex(myfile, layer, A, 0);
    }
    else{
        y = (b - sqrt(b*b - a*c)) / a;
        G = interval(y, C, interval(0.5, A, B));

        bulge1 = bulge(sub(A,G), v);
        bulge2 = bulge(w, sub(B,G));

        page_vertex(myfile, layer, A, -bulge1);
        page_vertex(myfile, layer, G, -bulge2);
    }
    return 1;
}
int bezier(ofstream &myfile, int layer, dpoint_t A, dpoint_t B, dpoint_t C, dpoint_t D){
    dpoint_t E = interval(0.75, A, B);
    dpoint_t G = interval(0.75, D, C);
    dpoint_t F = interval(0.5, E, G);

    pseudo_quad(myfile, layer, A, E, F);
    pseudo_quad(myfile, layer, F, G, D);
    return 1;
}
dpoint_t scalePoint(dpoint_t point, double scale) {
	point.x = point.x / scale;
	point.y = point.y / scale;
	return point;
}