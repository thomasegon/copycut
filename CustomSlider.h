#pragma once
using namespace System;
using namespace System::Drawing;
using namespace System::Windows::Forms;
using namespace System::Windows::Forms::VisualStyles;
ref class CustomTrackBar : public Control
{
private:
	System::Drawing::Rectangle trackRectangle;
	System::Drawing::Rectangle ticksRectangle;
	System::Drawing::Rectangle thumbRectangle;
	System::Drawing::Rectangle loadRectangle;
	System::Drawing::Rectangle truncatedRectangle;
	int currentTickPosition;
	int offset;
	float tickSpace, normalize, xOffset, yOffset, ticksOffset, trackHeight, trackWidth;
	bool thumbClicked;
	TrackBarThumbState thumbState;
	int  maximum, minimum, value, scale, thumbWidth, multiplier;
	System::Drawing::Size size;
	Brush^ mainColor;
	Brush^ secondColor;
	Brush^ thumbColor;
	Brush^ thumbSecondColor;
	Brush^ backgroundColor;

public:
	property Color MainColor {
		Color get() {
			return Pen(mainColor).Color;
		};
		void set(System::Drawing::Color value) {
			this->mainColor = gcnew SolidBrush(value);
		};
	}
	property Color SecondColor {
		Color get() {
			return Pen(secondColor).Color;
		};
		void set(System::Drawing::Color value) {
			this->secondColor = gcnew SolidBrush(value);
		};
	}
	property Color ThumbColor {
		Color get() {
			return Pen(thumbColor).Color;
		};
		void set(System::Drawing::Color value) {
			this->thumbColor = gcnew SolidBrush(value);
		};
	}
	property Color ThumbSecondColor {
		Color get() {
			return Pen(thumbSecondColor).Color;
		};
		void set(System::Drawing::Color value) {
			this->thumbSecondColor = gcnew SolidBrush(value);
		};
	}
	property Color BackgroundColor {
		Color get() {
			return Pen(backgroundColor).Color;
		};
		void set(System::Drawing::Color value) {
			this->backgroundColor = gcnew SolidBrush(value);
		};
	}
	bool roundThumb;
	property int Value {
		int get() {
			return this->value;
		};
		void set(int value) {
			if (value<=this->maximum && value>=this->minimum) {
				this->value = value;
				this->currentTickPosition = value;
				ValueChanging((Object^)this, System::EventArgs::Empty);
				thumbRectangle.X = CurrentTickXCoordinate();
				loadRectangle.Width = CurrentTickXCoordinate();
				Invalidate();
			}
		};
	}
	property int Scale {
		int get() {
			return this->scale;
		};
		void set(int value) {
			this->scale = value;
			SetupTrackBar();
		};
	}
	property int ThumbWidth {
		int get() {
			return this->thumbRectangle.Width;
		};
		void set(int value) {
			this->thumbWidth = value;
			SetupTrackBar();
		};
	}
	property System::Drawing::Size NewSize {
		System::Drawing::Size get() {
			return this->size;
		};
		void set(System::Drawing::Size value) {
			this->size = value;
			this->Size = value;
			SetupTrackBar();
		};
	}
	property int Maximum {
		int get() {
			return this->maximum;
		};
		void set(int value) {
			this->maximum = value;
			SetupTrackBar();	
		};
	}
	property int Minimum {
		int get() {
			return this-> minimum;
		};
		void set(int value) {
			this->minimum = value;
			SetupTrackBar();
		};
	}
	property int Multiplier {
		int get() {
			return this->multiplier;
		};
		void set(int value) {
			this->multiplier = value;
		};
	}
	CustomTrackBar()
	{
		this->Location = System::Drawing::Point(0, 0);
		this->DoubleBuffered = true;
		this->mainColor = Brushes::DarkGray;
		this->SecondColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(139)), static_cast<System::Int32>(static_cast<System::Byte>(159)),
				static_cast<System::Int32>(static_cast<System::Byte>(183)));
		this->thumbColor = Brushes::Black;
		this->thumbSecondColor = Brushes::White;
		this->roundThumb = false;
		this->minimum = 0; // Not working as it is now, because it is not implemented
		this->maximum = 100;
		this->value = 0;
		this->currentTickPosition = 0;
		this->offset = this->Location.X;
		this->scale = 5;
		this->multiplier = 1;

		// Calculate the initial sizes of the bar,
		// thumb and ticks.
		SetupTrackBar();
	}

	// Calculate the sizes of the bar, thumb, and ticks rectangle.
private:
	void SetupTrackBar()
	{
		// Calculate the size of the track bar.
		trackHeight = this->size.Height - scale;
		trackWidth = this->size.Width - scale;
		this->yOffset = (this->size.Height - trackHeight)/2;
		this->xOffset = (this->size.Width - trackWidth) / 2;

		
		trackRectangle.X = this->Location.X + xOffset;
		trackRectangle.Y = this->Location.Y + yOffset;
		trackRectangle.Width = trackWidth;
		trackRectangle.Height = trackHeight;
		this->normalize = (float)trackRectangle.Width / (float)maximum;

		// Calculate the size of the rectangle in which to
		// draw the ticks.
		this->ticksOffset = trackRectangle.Width / 25;
		ticksRectangle.X = trackRectangle.X;
		ticksRectangle.Y = trackRectangle.Y -8;
		ticksRectangle.Width = trackRectangle.Width;
		ticksRectangle.Height = 8;
		tickSpace = ((float)ticksRectangle.Width) /
			((float)maximum);

		loadRectangle.X = trackRectangle.X;
		loadRectangle.Y = trackRectangle.Y;
		loadRectangle.Height = trackRectangle.Height;
		loadRectangle.Width = CurrentTickXCoordinate();

		if(!thumbWidth)
			thumbRectangle.Size = System::Drawing::Size(trackRectangle.Height, trackRectangle.Height);
		else
			thumbRectangle.Size = System::Drawing::Size(thumbWidth, trackRectangle.Height);
		thumbRectangle.X = CurrentTickXCoordinate();
		thumbRectangle.Y = trackRectangle.Y;
		if (roundThumb) {
			thumbRectangle.Height += scale;
			thumbRectangle.Width = thumbRectangle.Height;
			thumbRectangle.Y = trackRectangle.Y - (scale / 2);
		}
	}

private:
	int CurrentTickXCoordinate()
	{
		if (tickSpace == 0)
		{
			return 0;
		}
		else
		{
			return (tickSpace *
				currentTickPosition) - ((float)thumbRectangle.Width / 2) + xOffset;
		}
	}

	// Draw the track bar.
protected:
	virtual void OnPaint(PaintEventArgs^ e) override
	{
		e->Graphics->FillRectangle(this->mainColor, trackRectangle);
		e->Graphics->FillRectangle(this->secondColor, loadRectangle);
		//Round corners
		//Pen^ whitePen = gcnew Pen(Color::White, 4);
		//array<Point>^curvePoints = { Point(trackRectangle.Width - 2,trackRectangle.Y),Point(trackRectangle.Width,trackRectangle.Y + (trackRectangle.Height / 2)),Point(trackRectangle.Width - 2,trackRectangle.Y + trackRectangle.Height) };
		//e->Graphics->DrawCurve(whitePen, curvePoints);
		//array<Point>^curvePointsFirst = { Point(trackRectangle.X,trackRectangle.Y),Point(trackRectangle.X-2,trackRectangle.Y + (trackRectangle.Height / 2)),Point(trackRectangle.X,trackRectangle.Y + trackRectangle.Height) };
		//e->Graphics->DrawCurve(whitePen, curvePointsFirst);
		//TrackBarRenderer::DrawHorizontalTicks(e->Graphics,
		//	ticksRectangle, maximum, EdgeStyle::Raised);
		if (roundThumb)
			e->Graphics->FillEllipse(thumbClicked ? this->thumbSecondColor : this->thumbColor, thumbRectangle);
		else
			e->Graphics->FillRectangle(thumbClicked ? this->thumbSecondColor : this->thumbColor, thumbRectangle);
	}

	// Determine whether the user has clicked the track bar thumb.
protected:
	virtual void OnMouseDown(MouseEventArgs^ e) override
	{
		if (this->thumbRectangle.Contains(e->Location))
		{
			thumbClicked = true;
		}
		else if(this->trackRectangle.Contains(e->Location))
		{
			if ((e->Location.X - xOffset) / normalize > minimum && (e->Location.X- xOffset) / normalize < maximum)
				currentTickPosition = Math::Round((float)(e->Location.X - xOffset) / normalize);
			else if ((e->Location.X - xOffset) / normalize <= minimum)
				currentTickPosition = minimum;
			else
				currentTickPosition = maximum;
			value = currentTickPosition;
			ValueChanging((Object^)this, e);
			thumbRectangle.X = CurrentTickXCoordinate();
			loadRectangle.Width = CurrentTickXCoordinate();
			thumbClicked = true;
		}
		this->Invalidate();
	}
public:
	event EventHandler^ ValueChanged;
	event EventHandler^ ValueChanging;
	// Redraw the track bar thumb if the user has moved it.
protected:
	virtual void OnMouseUp(MouseEventArgs^ e) override
	{
		if (thumbClicked == true)
		{
			thumbClicked = false;
			value = currentTickPosition;
			ValueChanged((Object^)this, e);
		}
	}
	// Track cursor movements.
protected:
	virtual void OnMouseMove(MouseEventArgs^ e) override
	{
		// The user is moving the thumb.
		if (thumbClicked == true)
		{
			if ((e->Location.X - xOffset) / normalize > minimum && (e->Location.X - xOffset) / normalize < maximum)
				currentTickPosition = Math::Round((float)(e->Location.X - xOffset) / normalize);
			else if ((e->Location.X - xOffset) / normalize <= minimum)
				currentTickPosition = minimum;
			else
				currentTickPosition = maximum;
			value = currentTickPosition;
			ValueChanging((Object^)this, e);
			thumbRectangle.X = CurrentTickXCoordinate();
			loadRectangle.Width = CurrentTickXCoordinate();
		}

		Invalidate();
	}
};