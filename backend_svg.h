#ifndef BACKEND_SVG_H
#define BACKEND_SVG_H
#include <iostream>
#include <fstream>
#include <string>
#include "HTML.h"
extern "C"
{
#include "lib_windows\main.h"
}

/// <summary> Creates a html string containing the svg from the paths found with potrace </summary>
/// <param name="*p"> paths found with potrace. Contains segemts of corners + curves </param>
/// <param name="width"> width of the image used for svg </param>
/// <param name="height"> height of the image used for svg and transform. Transform is needed because the svg has to be mirrored on the x axis </param>
/// <param name="worker"> The background worker running the process. Used to cancel the function before completion if cancellationPending is true</param>
/// <param name="e"> Used to set the cancel flag </param>
/// <returns> string containing the html for the browser </returns>
std::string makeSvg(potrace_path_t *p, int width, int height, System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e);


#endif // BACKEND_SVG_H