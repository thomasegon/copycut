#include "canon.h"

using namespace std;
using namespace cv;

bool bPhotoTaken = false;
Mat image;
//Function to take picture, can be expanded to use shutter button instead.
EdsError takePicture(EdsCameraRef camera) {
	return EdsSendCommand(camera, kEdsCameraCommand_TakePicture, 0);
}
//Function to download image from event or source
EdsError downloadImage(EdsDirectoryItemRef directoryItem, string filename) {
	EdsError err = EDS_ERR_OK;
	EdsStreamRef stream = NULL;

	// Get directory item information
	EdsDirectoryItemInfo dirItemInfo;
	err = EdsGetDirectoryItemInfo(directoryItem, &dirItemInfo);
	// Create file stream for transfer destination 
	if (err == EDS_ERR_OK)
	{
		err = EdsCreateFileStream(filename.c_str(), kEdsFileCreateDisposition_CreateAlways, kEdsAccess_ReadWrite, &stream);
	}
	// Download image
	if (err == EDS_ERR_OK) {
		err = EdsDownload(directoryItem, dirItemInfo.size, stream);
	}
	// Issue notification that download is complete 
	if (err == EDS_ERR_OK)
	{
		err = EdsDownloadComplete(directoryItem);
	}
	// Release stream
	if (stream != NULL) {
		EdsRelease(stream);
		stream = NULL;
	}
	return err;
}
//Fuction to get image(cv::mat) either by event or source.
EdsError getMat(EdsDirectoryItemRef directoryItem) {
	EdsError err = EDS_ERR_OK;
	unsigned char *data = NULL;
	// Get directory item information
	EdsStreamRef stream;
	EdsDirectoryItemInfo dirItemInfo;
	err = EdsGetDirectoryItemInfo(directoryItem, &dirItemInfo);
	// Create file stream
	if (err == EDS_ERR_OK)
	{
		err = EdsCreateMemoryStream(dirItemInfo.size, &stream);
		// Download image
		if (err == EDS_ERR_OK) {
			err = EdsDownload(directoryItem, dirItemInfo.size, stream);
		}
		// Issue notification that download is complete 
		if (err == EDS_ERR_OK)
		{
			err = EdsDownloadComplete(directoryItem);
		}
		
		EdsGetPointer(stream, (EdsVoid**)&data);
		Mat img(6000, 4000, CV_8UC4, data);
		//Saves image to a global variable so it is possible to get it in the main function.
		image = imdecode(img, CV_LOAD_IMAGE_COLOR);

		// Release stream
		if (stream != NULL) {
			EdsRelease(stream);
			stream = NULL;
		}
	}
	return err;
}
//Event for getPicture
EdsError EDSCALLBACK handleObjectEvent(EdsObjectEvent event, EdsBaseRef object, EdsVoid * context)
{
	EdsError err = EDS_ERR_OK;
	if (event == kEdsObjectEvent_DirItemRequestTransfer)
	{
		downloadImage(object, *static_cast<std::string*>(context));
		bPhotoTaken = true;
	}
	if (object) EdsRelease(object);
	return err;
}
//Event for getMatFromStream
EdsError EDSCALLBACK handleObjectEventStream(EdsObjectEvent event, EdsBaseRef object, EdsVoid * context)
{
	EdsError err = EDS_ERR_OK;
	if (event == kEdsObjectEvent_DirItemRequestTransfer)
	{
		getMat(object);
		bPhotoTaken = true;
	}
	if (object) EdsRelease(object);
	return err;
}
Mat getMatFromStream(bool debugging) {
	EdsCameraRef camera = NULL;
	EdsError err = EDS_ERR_OK;
	EdsCameraListRef cameraList = NULL;
	EdsUInt32 count = 0;
	EdsVolumeRef volume = NULL;
	EdsDirectoryItemRef directoryItem = NULL;
	EdsInt32 SaveTarget = kEdsSaveTo_Host;
	EdsCapacity inCapacity = { 0x7fffffff, 0x1000,1 };
	MSG msg;
	
	err = EdsInitializeSDK();
	err = EdsGetCameraList(&cameraList);
	err = EdsGetChildCount(cameraList, &count);
	if (count > 0)
	{
		err = EdsGetChildAtIndex(cameraList, 0, &camera);
		EdsRelease(cameraList);
	}
	if (err == EDS_ERR_OK) {
		err = EdsSetObjectEventHandler(camera, kEdsObjectEvent_All, handleObjectEventStream, NULL);
		if (err == EDS_ERR_OK && debugging) {
			cout << "Set object handler \n";
		}
	}
	if (err == EDS_ERR_OK) {
		err = EdsOpenSession(camera);
	}
	if (err == EDS_ERR_OK) {
		err = EdsSetPropertyData(camera, kEdsPropID_SaveTo, 0, sizeof(SaveTarget), &SaveTarget);
		if (err == EDS_ERR_OK && debugging) {
			cout << "Settings changed \n";
		}
	}
	if (err == EDS_ERR_OK) {
		err = EdsSetCapacity(camera, inCapacity);
		if (err == EDS_ERR_OK && debugging) {
			cout << "Set capacity changed \n";
		}
	}
	if (err == EDS_ERR_OK) {
		err = takePicture(camera);
		if (err == EDS_ERR_OK && debugging) {
			cout << "Picture captured \n";
		}
	}
	if (err == EDS_ERR_OK) {
		while ((GetMessage(&msg, NULL, 0, 0)) != 0)
		{
			if (GetMessage(&msg, NULL, 0, 0) == -1 || bPhotoTaken)
			{
				bPhotoTaken = false;
				break;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}
	if (err == EDS_ERR_OK) {
		err = EdsCloseSession(camera);
	}
	if (camera != NULL) {
		EdsRelease(camera);
	}
	EdsTerminateSDK();
	return image;
}
EdsError getPicture(string filename, bool debugging) {
	EdsCameraRef camera = NULL;
	EdsError err = EDS_ERR_OK;
	EdsCameraListRef cameraList = NULL;
	EdsUInt32 count = 0;
	EdsVolumeRef volume = NULL;
	EdsDirectoryItemRef directoryItem = NULL;
	EdsInt32 SaveTarget = kEdsSaveTo_Host;
	EdsCapacity inCapacity = { 0x7fffffff, 0x1000,1 };
	MSG msg;
	EdsVoid *context = &filename;

	err = EdsInitializeSDK();
	err = EdsGetCameraList(&cameraList);
	err = EdsGetChildCount(cameraList, &count);
	if (count > 0)
	{
		err = EdsGetChildAtIndex(cameraList, 0, &camera);
		EdsRelease(cameraList);
	}
	if (err == EDS_ERR_OK) {
		err = EdsSetObjectEventHandler(camera, kEdsObjectEvent_All, handleObjectEvent, context);
		if (err == EDS_ERR_OK && debugging) {
			cout << "Set object handler \n";
		}
	}
	if (err == EDS_ERR_OK) {
		err = EdsOpenSession(camera);
	}
	if (err == EDS_ERR_OK) {
		err = EdsSetPropertyData(camera, kEdsPropID_SaveTo, 0, sizeof(SaveTarget), &SaveTarget);
		if (err == EDS_ERR_OK && debugging) {
			cout << "Settings changed \n";
		}
	}
	
	if (err == EDS_ERR_OK) {
		err = EdsSetCapacity(camera, inCapacity);
		if (err == EDS_ERR_OK && debugging) {
			cout << "Set capacity changed \n";
		}
	}
	if (err == EDS_ERR_OK) {
		err = takePicture(camera);
		if (err == EDS_ERR_OK && debugging) {
			cout << "Picture captured \n";
		}
	}
	if (err == EDS_ERR_OK) {
		while ((GetMessage(&msg, NULL, 0, 0)) != 0)
		{
			if (GetMessage(&msg, NULL, 0, 0) == -1 || bPhotoTaken)
			{
				bPhotoTaken = false;
				break;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}
	if (err == EDS_ERR_OK) {
		err = EdsCloseSession(camera);
	}
	if (camera != NULL) {
		EdsRelease(camera);
	}
	EdsTerminateSDK();
	return err;
}
//Function for checking if there is a camera connected instead of taking a picture for checking.
EdsError checkForCamera() {
	EdsCameraRef camera = NULL;
	EdsError err = EDS_ERR_OK;
	EdsCameraListRef cameraList = NULL;
	EdsUInt32 count = 0;

	err = EdsInitializeSDK();
	err = EdsGetCameraList(&cameraList);
	err = EdsGetChildCount(cameraList, &count);
	if (count > 0)
	{
		err = EdsGetChildAtIndex(cameraList, 0, &camera);
		EdsRelease(cameraList);
	}
	if (camera != NULL) {
		EdsRelease(camera);
	}
	EdsTerminateSDK();
	return EDS_ERR_DEVICE_INVALID ? camera == NULL : err;
}