//#include <iostream>
//#include <fstream>
//#include "opencv2/core/core.hpp"
//#include "opencv2/video/video.hpp"
//#include "opencv2/imgcodecs/imgcodecs.hpp"
//
//#include "PolylineUnion.h"
//#include "ImageManipulation.h"
//#include "canon.h"
//#include "XML.h"
//#include <vector>
//
//#include "backend_dxf.h"
//#include "Argument.h"
//#if __APPLE__
//    extern "C"
//    {
//    #include "./lib/bitmap.h"
//    #include "./lib/bitmap_io.h"
//    }
//#elif __linux__
//    extern "C"
//    {
//    #include "./lib_ubuntu/bitmap.h"
//    #include "./lib_ubuntu/bitmap_io.h"
//    }
//#else
//	extern "C"
//	{
//		#include "bitmap.h"
//		#include "bitmap_io.h"
//	}
//#endif
//
//using namespace cv;
//using namespace std;
//
//
//int main(int argc, char *argv[])
//{
	//// Default values
	//string file_name = "test.jpg", dxf_name = "example.dxf"; // example image
	//double binary_thres = 180, binary_max = 255, scale = 2.348;
	//int morph_iterations = 0, thickness = 30;
	//bool pelican = false, otsu = false, debugging = true, forceCalibration = false, calibration = false, background = false, invert = true, bedROI = false;
	//float ratio = 1.0;
	//potrace_param_t *param = potrace_param_default();
//
//	// Parse and assign arguments given from command line
//	parse_arguments(argc, argv, file_name, binary_thres, binary_max, morph_iterations, otsu, *param, pelican, debugging, dxf_name, forceCalibration, scale, thickness, background);
//
//
//	if (debugging) {
//		cout << "file_name: " << file_name << endl;
//		cout << "binary_thres: " << binary_thres << endl;
//		cout << "binary_max: " << binary_max << endl;
//		cout << "morph_iterations: " << morph_iterations << endl;
//		cout << "otsu: " << otsu << endl;
//		cout << "pelican: " << pelican << endl;
//		cout << "debugging: " << debugging << endl;
//		cout << "dxf_name: " << dxf_name << endl;
//		cout << "forceCalibration: " << forceCalibration << endl;
//		cout << "alphamax: " << param->alphamax << endl;
//		cout << "opticurve: " << param->opticurve << endl;
//		cout << "opttolerance: " << param->opttolerance << endl;
//		cout << "turdsize: " << param->turdsize << endl;
//		cout << "turnpolicy: " << param->turnpolicy << endl << endl;
//	}
//
//
//
//	ofstream myfile;
//	myfile.open(dxf_name.c_str());
//	FILE *bmfile = new FILE;
//	bmfile = fopen("bm.pgm", "wb");
	//EdsError err = EDS_ERR_OK;



	//potrace_bitmap_t *bm = new potrace_bitmap_t;
	//potrace_path_t *p = new potrace_path_t;
	//potrace_state_t *st = new potrace_state_t;
	//imginfo_t *imginfo = new imginfo_t;
	//int n, *tag;
	//potrace_dpoint_t(*c)[3];
	//vector<Point> ROI = vector<Point>{};
//
//	Mat image, cameraMatrix, distCoeffs, undistorted;
//	if (checkForCamera() == EDS_ERR_OK && calibration) {
//
//
//		if (debugging) {
//			cout << "calibrated\n";
//			cout << "px/mm = " << ratio << endl;
//		}
//	}
//
	//if (checkForCamera() == EDS_ERR_OK) {
	//	if (!background) {
	//		image = getMatFromStream(debugging);
	//	}
	//	else {
	//		image = BackgroundSubtraction(debugging);
	//	}
	//}

	//if (image.empty()) {
	//	err = EDS_ERR_COMM_DISCONNECTED;
	//}
	//if (err != EDS_ERR_OK) {
	//	image = imread("IMG_0389.JPG", CV_LOAD_IMAGE_COLOR);
	//	cout << "Camera is not connected\n";
//	}
//	int WIDTH = image.cols;
//	int HEIGHT = image.rows;
//	if (debugging)
//	{
//		cout << "Original Image\n";
//		vector<Mat> images = { image };
//		Showim(images, true, "Original Image");
//	}
//
//	if (err == EDS_ERR_OK && calibration) {
//		undistort(image, undistorted, cameraMatrix, distCoeffs);
//		image = undistorted;
//		if (debugging)
//		{
//			cout << "undistorted image\n";
//			Showim(vector<Mat>{image}, true, "undistorted");
//		}
//	}
//
//
//
//
//	//Clahe(&image);
//	//if (debugging)
//	//{
//	//	cout << "Clahe\n";
//	//	Showim(&image, 1000, "Clahe");
//	//}
//
//	Blurring(&image, morph_iterations);
//
//	if (debugging)
//	{
//		cout << "After Blurring\n";
//		Showim(vector<Mat>{image}, true, "After Blurring");
//	}
//
//	if (otsu)
//	{
//		Otsu(&image, 70, 255);
//		if (debugging)
//		{
//			cout << "After Otsu\n";
//			Showim(vector<Mat>{image}, true, "After Otsu");
//		}
//	}
//
//	Binary(&image, binary_thres, binary_max);
//	if (debugging)
//	{
//		cout << "After Binary Threshold\n";
//		Showim(vector<Mat>{image}, true, "After Binary Threshold");
//	}
//
//	if (bedROI) {
//		FindROI(&image, 175, 2050, 3350, 3625);
//	}
//	else
//		while (ROI.size() == 0) {
//			ROI = drawOnImage(image, "FindROI");
//		}
//	FindROI(&image, ROI);
//
//	DeleteSmallContours(&image, 3);
//	Showim(image, true, "after delete");
//
//	//FindROI(&image, 175, 2050, 3350, 3625);
//	if (debugging)
//	{
//		cout << "After ROI\n";
//		Showim(vector<Mat>{image}, true, "After ROI");
//	}
//	//image = aproxPoly(image, 4);
//
//	if (invert) {
//		invertImage(&image);
//		if (debugging)
//		{
//			cout << "invert Image\n";
//			Showim(image, true, "invert Image");
//		}
//	}
//
//
//
//	if (pelican) {
//		vector<vector<Point> > peli = FindPelicanApprox(image, 50);
//		image = Mat(Size(image.cols, image.rows), CV_8UC3);
//		drawContours(image, peli, -1, Scalar(255, 255, 255), thickness);
//	}
//
//
//
//	Showim(image, true, "aprox");
//
//	Mat image_bmp;
//	image.convertTo(image_bmp, CV_8UC3);
//	imwrite("out.bmp", image_bmp);
//	FILE *fin = new FILE;
//	fin = fopen("out.bmp", "rb");
//	remove("out.bmp");
//	bm_read(fin, 0.5, &bm);
//	fclose(fin);
//
//	bm_writepbm(bmfile, bm);
//	fclose(bmfile);
//	st = potrace_trace(param, bm);
//	bm_free(bm);
//	p = st->plist;
//
//	page_header(myfile, WIDTH, HEIGHT);
//	p = st->plist;
//	while (p != NULL) {
//		n = p->curve.n;
//		dpoint_t *c, *c1;
//		page_polyline(myfile);
//		for (int i = 0;i < n;i++) {
//			c = p->curve.c[i];
//			c1 = p->curve.c[mod(i - 1, n)];
//			switch (p->curve.tag[i]) {
//			case POTRACE_CORNER:
//				page_vertex(myfile, 0, scalePoint(c1[2], scale), 0);
//				page_vertex(myfile, 0, scalePoint(c[1], scale), 0);
//				break;
//			case POTRACE_CURVETO:
//				bezier(myfile, 0, scalePoint(c1[2], scale), scalePoint(c[0], scale), scalePoint(c[1], scale), scalePoint(c[2], scale));
//				break;
//			}
//		}
//		page_seqend(myfile);
//		if (false) {
//			p = p->sibling;
//		}
//		else {
//			p = p->next;
//		}
//
//	}
//	page_end(myfile);
//	myfile.close();
//	potrace_state_free(st);
//	cout << "Finshed!";
//	getchar();
//	return 0;
//}