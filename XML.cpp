#include "XML.h"

using namespace cv;
using namespace std;

bool XML_file_exists() {
	// This function might not be needed if cv::filestorage can handle missing files by itself.
	// Otherwise this check needs to be done in order to create an xml file before cv:filestorage is used
	if (FILE *file = fopen("Settings.xml", "r")) {
		fclose(file);
		return true;
	}
	else {
		return false;
	}
}


void Write_Settings_To_XML(int turdSize, int alphaMax, int tolerance, bool blurring, int morph_iterations, 
							double binary_thres, double scale, bool optimize, bool invert, int roi1x, int roi1y, int roi2x, int roi2y) {
	FileStorage fs("Settings.xml", FileStorage::WRITE);
	fs << "turdSize" << turdSize;
	fs << "alphaMax" << alphaMax;
	fs << "tolerance" << tolerance;
	fs << "blurring" << blurring;
	fs << "morph_iterations" << morph_iterations;
	fs << "scale" << scale;
	fs << "binary_thres" << binary_thres;
	fs << "optimize" << optimize;
	fs << "invert" << invert;
	fs << "roi1x" << roi1x;
	fs << "roi1y" << roi1y;
	fs << "roi2x" << roi2x;
	fs << "roi2y" << roi2y;
	fs.release();
	
}

