#ifndef IMAGEMANIPULATION_H
#define IMAGEMANIPULATION_H
#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/video/background_segm.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <string>
#include "canon.h"
#include "XML.h"
#include <math.h>
#include <algorithm>

/// <summary> Blurring smooths out the image by using morphologyEx from Opencv </summary>
/// <param name="src"> The input/output image to be blurred </param>
/// <param name="iterations"> How many blurring iterations to do. More iterations will smooth out the image more, removing jagged lines, but also rounding corners</param>
/// <param name="worker"> The background worker running the process. Used to for cancellation before the function is finished </param>
/// <param name="e"> DoWorkEventArgs used to set the cancel flag </param>
/// <returns> Void </returns>
void Blurring(cv::Mat *src, int iterations, System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e);

/// <summary> Otsu is a "smart" binary threshold, which will attempt to find the threshold automatically </summary>
/// <param name="src"> The input/output image to be blurred </param>
/// <param name="thres"> The initial threshold parameter, but will otherwise not affect the result </param>
/// <param name="maxvalue"> The max pixel value. Should always be 255. Lower values will only make the image lighter </param>
/// <returns> Void </returns>
void Otsu(cv::Mat *src, int thresh, double maxvalue);

/// <summary> Binary threshold, which is used to make a gray image into a binary image with only black and white for better contrast </summary>
/// <param name="src"> The input/output image to be blurred </param>
/// <param name="value"> The treshold value, which spceify at what pixel value a pixel will be consider either black or white </param>
/// <param name="maxvalue"> The max pixel value. Should always be 255. Lower values will only make the image lighter </param>
/// <returns> Void </returns>
void Binary(cv::Mat *src, double value, double maxvalue);

void Dilation(cv::Mat *src, int iterations);

void Erotion(cv::Mat *src, int iterations);

void Showim(std::vector<cv::Mat> images, bool resize, std::string name, double factor=2);

void Showim(cv::Mat image, bool resize, std::string name, double factor=2);

void Clahe(cv::Mat *src);

void FindROI(cv::Mat *src, int point1_x, int point1_y, int point2_x, int point2_y);

void FindROI(cv::Mat *src, std::vector<cv::Point> points);

cv::Mat BackgroundSubtraction(bool debug);

void CameraCalibrationPictures(int numCornersHorizontal, int numCornersVertical, int square_size, std::string path, bool debug);

int CameraCalibration(int numBoards, int numCornersHorizontal, int numCornersVertical, int square_size, float &ratio, cv::Mat *cameraMatrix, cv::Mat *distCoeffsret, bool forceCalibration, bool debug, int focalLength);

double FindPaperLongSide(cv::Mat image, double binary);

std::vector<std::vector<cv::Point> > FindPelicanApprox(cv::Mat image, double epsilon, bool debugging = false);

void ShadowRemoval(cv::Mat *img, int blurIterations, int dilateKernelSize);

void invertImage(cv::Mat *image);

std::vector<cv::Point> drawOnImage(cv::Mat image, std::string imageName = "test", bool resize = true, double factor = 1.15);

void DeleteSmallContours(cv::Mat *Image, int numberOfItems);

cv::Mat aproxPoly(cv::Mat image, double epsilon);

#endif