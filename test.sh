#!/bin/bash

function help_text(){
    echo -e "USAGE: $FUNCNAME [OPTIONS]=[TEXT] \n"
    echo -e "OPTIONS"
    echo -e "  -h | --help \t \t \t Gets this help"
    echo -e "  -i | --image \t \t \t Path to the image"
    echo -e "  -t | --binary_thres \t \t Binary Threshold"
    echo -e "  -b | --binary_max \t \t Max Binary Value"
    echo -e "  -m | --morph_iterations \t Morph Iterations"
    echo -e "  -s | --turdsize \t \t Turdsize"
    echo -e "     | --turnpolicy \t \t Turnpolicy (black, white, left, right, minority, majority, random)"
    echo -e "  -a | --alphamax \t \t Alpha Max"
    echo -e "  -c | --opticurve \t \t Opticurve"
    echo -e "  -o | --opttolerance \t \t Opttolerance"
    echo -e "       --otsu \t \t \t Disable Otsu"
    echo -e "  -p | --pelican \t \t Enable Pelican mode"
    echo -e "  -d | --debug \t \t Enable debugging"
    echo -e "       --dxf \t \t Name of dxf file"
    echo -e "       --out \t \t Name of out file"
}
PARAMETERS=""
DXFFILE="test.dxf"
OUTFILE="test.out"
while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            help_text
            exit
            ;;
        -i | --image)
            if [ ! -z "$VALUE" ]; then
                PARAMETERS=$(printf "%s image=%s" "$PARAMETERS" "$VALUE")
            fi
            ;;
        -t | --binary_thres)
            if [ ! -z "$VALUE" ]; then
                PARAMETERS=$(printf "%s binary_thres=%s" "$PARAMETERS" "$VALUE")
            fi
            ;;
        -b | --binary_max)
            if [ ! -z "$VALUE" ]; then
                PARAMETERS=$(printf "%s binary_max=%s" "$PARAMETERS" "$VALUE")
            fi
            ;;
        -m | --morph_iterations)
            if [ ! -z "$VALUE" ]; then
                PARAMETERS=$(printf "%s morph_iterations=%s" "$PARAMETERS" "$VALUE")
            fi
            ;;
        -s | --turdsize)
            if [ ! -z "$VALUE" ]; then
                PARAMETERS=$(printf "%s turdsize=%s" "$PARAMETERS" "$VALUE")
            fi
            ;;
        --turnpolicy)
            if [ ! -z "$VALUE" ]; then
                PARAMETERS=$(printf "%s turnpolicy=%s" "$PARAMETERS" "$VALUE")
            fi #MAKE MORE HERE
            ;;
        -a | --alphamax)
            if [ ! -z "$VALUE" ]; then
               PARAMETERS=$(printf "%s alphamax=%s" "$PARAMETERS" "$VALUE")
            fi
            ;;
        -c | --opticurve)
            if [ ! -z "$VALUE" ]; then
                PARAMETERS=$(printf "%s opticurve=%s" "$PARAMETERS" "$VALUE")
            fi
            ;;
        -o | --opttolerance)
            if [ ! -z "$VALUE" ]; then
                PARAMETERS=$(printf "%s opttolerance=%s" "$PARAMETERS" "$VALUE")
            fi
            ;;
        --otsu)
            if [ "$VALUE" = "false" ] || [ "$VALUE" = "0" ] || [ -z "$VALUE" ]; then
                PARAMETERS=$(printf "%s otsu=false" "$PARAMETERS")
            else
                echo -e "Value should be either false, 0 or nothing for disabling otsu"
                exit 1
            fi
            ;;
        -p | --pelican)
            if [ "$VALUE" = "true" ] || [ "$VALUE" = "1" ] || [ -z "$VALUE" ]; then
                PARAMETERS=$(printf "%s pelican=true" "$PARAMETERS")
            else
                echo -e "Value should be either true, 1 or nothing for enabling Pelican mode"
                exit 1
            fi
            ;;
        -d | --debug)
            if [ "$VALUE" = "true" ] || [ "$VALUE" = "1" ] || [ -z "$VALUE" ]; then
                PARAMETERS=$(printf "%s debug=true" "$PARAMETERS")
            else
                echo -e "Value should be either true, 1 or nothing for enabling debugging"
                exit 1
            fi
            ;;
        --dxf)
            if [ ! -z "$VALUE" ]; then
                PARAMETERS=$(printf "%s dxf=%s" "$PARAMETERS" "$VALUE")
            fi
            ;;
        --out)
            if [ ! -z "$VALUE" ]; then
                OUTFILE=$(printf "%s" "$VALUE")
            fi
            ;;
            
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            help_text
            exit 1
            ;;
    esac
    shift
done

if [[ "$OSTYPE" == "linux-gnu" ]]; then
        g++  test.cpp backend_dxf.cpp ImageManipulation.cpp ./lib_ubuntu/libpotrace.a -o $OUTFILE `pkg-config --cflags --libs opencv`
elif [[ "$OSTYPE" == "darwin"* ]]; then
        g++ $(pkg-config --cflags --libs opencv) test.cpp backend_dxf.cpp ImageManipulation.cpp ./lib/libpotrace.a -o $OUTFILE
else
        g++  test.cpp backend_dxf.cpp ImageManipulation.cpp ./lib_windows/libpotrace.a -o $OUTFILE `pkg-config --libs --cflags opencv`
fi
./$OUTFILE $PARAMETERS

