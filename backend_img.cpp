#include "backend_img.h"
using namespace std;

string makeImg(System::Drawing::Bitmap^ image) {
	string html = "";
	
	image->Save("imageToShow.JPG", System::Drawing::Imaging::ImageFormat::Jpeg);
	//System::IO::MemoryStream^ ms = gcnew System::IO::MemoryStream();
	//image->Save(ms, System::Drawing::Imaging::ImageFormat::Bmp);
	//msclr::interop::marshal_context context;
	//html += "<img src='data:image / png;base64, "
	//+ context.marshal_as<std::string>(gcnew System::String(System::Convert::ToBase64String(ms->ToArray())))
	//+ "'/>";
	string width = image->Width > image->Height ? "width='100%' />" : "height='100%' />";
	html = "<img src='" + workingdir() + "imageToShow.jpg' style='margin: auto;' " + width;
	string ret = createHTML(html);
	return ret;
}