#ifndef CANON_H
#define CANON_H
#include "EDSDK.h"
#include "EDSDKErrors.h"
#include "EDSDKTypes.h"
#include <windows.h>
#include <string>
#include <iostream>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

EdsError getPicture(std::string filename, bool debugging);
cv::Mat getMatFromStream(bool debugging);
EdsError checkForCamera();

#endif // CANON_H