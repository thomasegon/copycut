#ifndef POLYLINEUNION_H
#define POLYLINEUNION_H

#include <boost/geometry/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <vector>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/video/background_segm.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <iostream>



std::vector<std::vector<cv::Point> > PolyUnion(std::vector<std::vector<cv::Point> > AproxContour, std::vector<std::vector<cv::Point> > Contour);
#endif