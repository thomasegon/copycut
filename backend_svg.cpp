#include "backend_svg.h"


using namespace std;
string svgCircle(string circles, double x, double y, int height, string color) {
	int radius = 1;
	return circles + "<circle cx =" + to_string(x)
		+ " cy = " + to_string(y)
		+ " r=" + to_string(radius)
		+ " stroke-opacity=\"0\" fill =\"" + color + "\""
		+ " transform = \"scale(1, -1) translate(0, " + to_string(-height) + ")\" /> \n";

}

string makeSvg(potrace_path_t *p, int width, int height, System::ComponentModel::BackgroundWorker^ worker, System::ComponentModel::DoWorkEventArgs^ e) {
	string pointColor = "red"; // used for points
	string controlColor = "blue"; // used for control points on bezier curves
	string html = "";
	string circles = "";

	html = html + "<svg id=\"svg\" version=\"1.1\" " + "viewbox=\"0 0 " + to_string(width) + " " + to_string(height) + "\" style=\"display: inline; width: inherit; min - width: inherit; max - width: inherit; height: inherit; min - height: inherit; max - height: inherit; \" "
		   + " xmlns=\"http://www.w3.org/2000/svg\"><path d=\"";
	int n, *tag;

	while (p != NULL) {
		n = p->curve.n;
		dpoint_t *c, *c1;
		c = p->curve.c[n - 1];
		// move to start point, which is the last endpoint
		html = html + "\n" + "M " + to_string(c[2].x) + " " + to_string(c[2].y); 
		circles = svgCircle(circles, c[2].x, c[2].y, height, pointColor);

		// loop over all curves in a segment. For each curve we check type and draw cornor/curve and points.
		for (int i = 0; i < n; i++) {
			//whether the process should be canceled 
			if (worker->CancellationPending)
			{
				worker->ReportProgress(1); // tell progress it process is canceled
				e->Cancel = true;
				return "";
			}
			c = p->curve.c[i];
			switch (p->curve.tag[i]) {
			case POTRACE_CORNER:
				html = html + "\n" + "L " + to_string(c[1].x) + " " + to_string(c[1].y) + " " + to_string(c[2].x) + " " + to_string(c[2].y);
				circles = svgCircle(circles, c[1].x, c[1].y, height, pointColor);
				circles = svgCircle(circles, c[2].x, c[2].y, height, pointColor);
				break;
			case POTRACE_CURVETO:
				html = html + "\n" + "C " + to_string(c[0].x) + " " + to_string(c[0].y) //control point 1
					+ " " + to_string(c[1].x) + " " + to_string(c[1].y)	// control point 2
					+ " " + to_string(c[2].x) + " " + to_string(c[2].y); // end point
				circles = svgCircle(circles, c[0].x, c[0].y, height, controlColor); // draw control point 1 as dot
				circles = svgCircle(circles, c[1].x, c[1].y, height, controlColor); // draw control point 2 as dot
				circles = svgCircle(circles, c[2].x, c[2].y, height, pointColor); // draw end point as dot
				break;
			}
		}
		// go to next segment
		p = p->next;
		

	}
	html = html + " \"" + " transform=\"scale(1, -1) translate(0," + to_string(-height) + ")\" " + "\n" // transform since it is mirrored
		+ " stroke=\"black\" fill-opacity=\"0.0\"  fill-rule=\"evenodd\" />";
	html = html + "\n" + circles; // add circles
	html = html + "</svg>" + "\n";
	return createHTML(html);
}