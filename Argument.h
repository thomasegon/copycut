#ifndef ARGUMENT_H
#define ARGUMENT_H
#include <string>

extern "C"
{
#include "bitmap.h"
#include "bitmap_io.h"
}

void parse_arguments(int argc, char *argv[], std::string &file_name,
	double &binary_thres, double &binary_max,
	int &morph_iterations, bool &otsu,
	potrace_param_t &potrace, bool &pelican,
	bool &debugging, std::string &dxf_name,
	bool &forceCalibration, double &scale, int &thickness, bool &background);
#endif