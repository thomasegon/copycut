#include "Argument.h"

using namespace std;

void parse_arguments(int argc, char *argv[], string &file_name, double &binary_thres, double &binary_max, int &morph_iterations, bool &otsu, potrace_param_t &potrace, bool &pelican, bool &debugging, string &dxf_name, bool &forceCalibration, double &scale, int &thickness, bool &background)
{

	for (int i = 1; i < argc; i++)
	{
		string argument = string(argv[i]);
		int split = argument.find("=");
		string parameter = argument.substr(0, split);
		string value = argument.substr(split + 1);

		if (parameter.compare("--image") == 0 || parameter.compare("-i") == 0)
		{
			file_name = value;
		}
		else if (parameter.compare("--binary_thres") == 0 || parameter.compare("-t") == 0)
		{
			binary_thres = atof(value.c_str());
		}
		else if (parameter.compare("--binary_max") == 0 || parameter.compare("-b") == 0)
		{
			binary_max = atof(value.c_str());
		}
		else if (parameter.compare("--morph_iterations") == 0 || parameter.compare("-m") == 0)
		{
			morph_iterations = atoi(value.c_str());
		}
		else if (parameter.compare("--scale") == 0 || parameter.compare("-b") == 0)
		{
			scale = atof(value.c_str());
		}
		else if (parameter.compare("--thickness") == 0 || parameter.compare("-m") == 0)
		{
			thickness = atoi(value.c_str());
		}
		else if (parameter.compare("--otsu") == 0)
		{
			if (value.compare("false") == 0 || value.compare("0") == 0)
			{
				otsu = false;
			}
		}
		else if (parameter.compare("--debug") == 0 || parameter.compare("-d") == 0)
		{
			if (value.compare("true") == 0 || value.compare("1") == 0 || value.compare("") == 0)
			{
				debugging = true;
			}
		}
		else if (parameter.compare("--pelican") == 0 || parameter.compare("-p") == 0)
		{
			if (value.compare("true") == 0 || value.compare("1") == 0)
			{
				pelican = true;
			}
		}
		else if (parameter.compare("--dxf") == 0)
		{
			dxf_name = value;
		}
		else if (parameter.compare("--calibration") == 0)
		{
			if (value.compare("true") == 0 || value.compare("1") == 0)
			{
				forceCalibration = true;
			}
		}
		else if (parameter.compare("--background") == 0)
		{
			if (value.compare("true") == 0 || value.compare("1") == 0)
			{
				background = true;
			}
		}
		//intturdsize; intturnpolicy; doublealphamax; intopticurve; doubleopttolerance; potrace_progress_tprogress;
		else if (parameter.compare("--turdsize") == 0 || parameter.compare("-s") == 0)
		{
			potrace.turdsize = atoi(value.c_str());
		}
		else if (parameter.compare("--turnpolicy") == 0)
		{
			int option;
			if (value.compare("black") == 0)
			{
				option = POTRACE_TURNPOLICY_BLACK;
			}
			else if (value.compare("white") == 0)
			{
				option = POTRACE_TURNPOLICY_WHITE;
			}
			else if (value.compare("left") == 0)
			{
				option = POTRACE_TURNPOLICY_LEFT;
			}
			else if (value.compare("right") == 0)
			{
				option = POTRACE_TURNPOLICY_RIGHT;
			}
			else if (value.compare("minority") == 0)
			{
				option = POTRACE_TURNPOLICY_MINORITY;
			}
			else if (value.compare("majority") == 0)
			{
				option = POTRACE_TURNPOLICY_MAJORITY;
			}
			else if (value.compare("random") == 0)
			{
				option = POTRACE_TURNPOLICY_RANDOM;
			}
			potrace.turnpolicy = option;
		}
		else if (parameter.compare("--alphamax") == 0 || parameter.compare("-a") == 0)
		{
			potrace.alphamax = atof(value.c_str());
		}
		else if (parameter.compare("--opticurve") == 0 || parameter.compare("-c") == 0)
		{
			potrace.opticurve = atoi(value.c_str());
		}
		else if (parameter.compare("--opttolerance") == 0 || parameter.compare("-o") == 0)
		{
			potrace.opttolerance = atof(value.c_str());
		}

		/* not included since input is more complex than simple number
		else if (parameter.compare("potrace_progress") == 0)
		{
		potrace.progress =
		}
		*/

	}

}