#include "PolylineUnion.h"

using namespace std;
typedef boost::geometry::model::d2::point_xy<double> PointType;
typedef boost::geometry::model::polygon<PointType> PolygonType;

PolygonType ToPolygon(vector<cv::Point> Contour) {
	//Converts a contour of std::vector<cv::Point>
	//to a boost::geometry::model::polygon<boost::geometry::model::d2::point_xy<double>>
	// and returns the polygon
	PolygonType Polygon;
	for (size_t i = 0; i < Contour.size(); i++)
	{
		Polygon.outer().push_back(PointType(Contour[i].x, Contour[i].y));
	}
	return Polygon;
}

vector<cv::Point> ToContour(PolygonType Polygon) {
	//Converts boost::geometry::model::polygon<boost::geometry::model::d2::point_xy<double>>
	//to a contour of the type std::vector<cv::Point>  
	vector<cv::Point> Contour;
	for (size_t i = 0; i < Polygon.outer().size(); i++)
	{
		Contour.push_back(cv::Point(Polygon.outer()[i].x(), Polygon.outer()[i].y()));
	}
	return Contour;
}


vector<vector<cv::Point> > PolyUnion(vector< vector<cv::Point> >AproxContour, vector< vector<cv::Point> > Contour) {
	// Creates a pairwise union between 2 vectors of contours.
	// The vectors must be of an equal length.
	// Returns a vector the unioned contours
	vector<vector<cv::Point> > Result;
	for (size_t i = 0; i < AproxContour.size(); i++) {

		//If the Contours has less than 3 vertices they can not be used for a union and is therefore skipped
		if (AproxContour[i].size() > 2 && Contour[i].size() > 2) {
			// Converts the contours to polygons
			PolygonType AproxPolygon = ToPolygon(AproxContour[i]);
			PolygonType Contourpolygon = ToPolygon(Contour[i]);

			// Corrects the polygons, for example closes the contours
			boost::geometry::correct(AproxPolygon);
			boost::geometry::correct(Contourpolygon);

			//Creates the union between the 2 polygons
			vector<PolygonType> Output;
			boost::geometry::union_(AproxPolygon, Contourpolygon, Output);
			
			//Checks if the union was successful, if it was successful it is added to the vector of results
			if(Output.size() > 0){
				Result.push_back(ToContour(Output[0]));
			}
		}
	}

	return Result;
}




