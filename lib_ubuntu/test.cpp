#include <iostream>
#include "potracelib.h"
#include "bitmap.h"
#include "curve.h"
#include "decompose.h"
#include "trace.h"
#include "progress.h"
#include "lists.h"

int main()
{
    potrace_bitmap_t *bm;
    potrace_param_t *param;
    potrace_path_t *p;
    potrace_state_t *st;
    bm = bm_new(100, 100);
    st = potrace_trace(potrace_param_default(), bm);

    std::cout << potrace_version() << std::endl;
    return 0;
}