#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include "opencv2/core/core.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/imgcodecs/imgcodecs.hpp"

#include "ImageManipulation.h"
#include "canon.h"
#include "XML.h"
#include <vector>
#include <cliext\vector>
#include <msclr\marshal_cppstd.h>

#include "backend_dxf.h"
#include "Argument.h"
#include "CustomSlider.h"
#include "CustomCheckboxButton.h"
#include "backend_svg.h"
#include "backend_img.h"


extern "C"
{
#include "bitmap.h"
#include "bitmap_io.h"
}
namespace cpotrace {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace std;
	using namespace cv;
	cv::Mat image;
	cv::Mat originalImage;
	cv::Mat UnCropped;


	ref class UpdateResults {
	public:
		System::String^ svghtml;
		Bitmap^ bmp;
	};
	

	/// <summary>
	/// Summary for Main
	/// </summary>
	public ref class Main : public System::Windows::Forms::Form
	{
	private:
			 double binary_thres = 180, binary_max = 255, scale = 2.348;
			 int morph_iterations = 0, TogMove, MValX, MValY, count, numberOfObjects, alphamax=100, tolerance=20, turdSize=2;
			 bool otsu = false, debugging = true, calibration = false, background = false, invert = false, blurring = false,
		 		 originalZooming, manipulatedZooming, mouseDown, removeSmallObjects, optimize=false, requestForDxf = false;
			 cliext::vector<System::Drawing::Point> ROI;
			 System::String^ filepath;
			 UpdateResults^ updateResults = gcnew UpdateResults();

			 potrace_param_t *param = potrace_param_default();
			 EdsError err = EDS_ERR_OK;

			 System::Drawing::Point^ originalScrollPos = gcnew System::Drawing::Point(0, 0);
			 System::Drawing::Point^ manipulatedScrollPos = gcnew System::Drawing::Point(0, 0);

			 System::Drawing::Point^ mouseDownPosition;
			 System::Drawing::Size^ originalImagePictureBoxSize;
			 System::Drawing::Size^ manipulatedImagePictureBoxSize;
	private: System::Windows::Forms::Panel^  topPanel;
	private: System::Windows::Forms::Panel^  logoPanel;
	private: System::Windows::Forms::Panel^  leftPanel;
	private: System::Windows::Forms::Panel^  optionsPanel;
	private: System::Windows::Forms::Panel^  imageManipulationPanel;
	private: System::Windows::Forms::Panel^  imageManipulationButtonPanel;
	private: System::Windows::Forms::Button^  imageManipulationButton;
	private: System::Windows::Forms::Panel^  imageManipulationSettingPanel;
	private: System::Windows::Forms::Panel^  ROIPanel;
	private: System::Windows::Forms::Button^  findROIButton;
	private: System::Windows::Forms::Panel^  objectsPanel;
	private: System::Windows::Forms::Panel^  objectsSettingsPanel;
	private: System::Windows::Forms::Panel^  objectsButtonPanel;
	private: System::Windows::Forms::Button^  contourButton;
	private: System::Windows::Forms::TableLayoutPanel^  mainTableLayoutPanel;
	private: System::Windows::Forms::TableLayoutPanel^  ROITableLayoutPanel;
	private: System::Windows::Forms::Panel^  contourImagePanel;
	private: System::Windows::Forms::Label^  contourImageLabel;
	private: System::Windows::Forms::Panel^  originalImagePanel;
	private: System::Windows::Forms::Label^  originalImageLabel;
	private: System::Windows::Forms::Panel^  manipulatedImagePanel;
	private: System::Windows::Forms::Label^  manipulatedImageLabel;
	private: System::Windows::Forms::Label^  closeLabel;
	private: System::Windows::Forms::Panel^  takePicturePanel;
	private: System::Windows::Forms::Panel^  calibrateCameraPanel;
	private: System::Windows::Forms::Button^  takePictureButton;
	private: System::Windows::Forms::Button^  calibrateCameraButton;
	private: System::Windows::Forms::Panel^  originalImageContainer;
	private: System::Windows::Forms::WebBrowser^  originalImageBrowser;
	private: System::Windows::Forms::Panel^  manipulatedImageContainer;
	private: System::Windows::Forms::WebBrowser^  manipulatedImageBrowser;
	private: System::Windows::Forms::Panel^  binaryThresholdSettingsPanel;
	private: System::Windows::Forms::Panel^  binaryThresholdSettingsContainer;
	private: System::Windows::Forms::Panel^  binaryThresholdButtonPanel;
	private: System::Windows::Forms::Button^  binaryThresholdSettingsButton;
	private: System::Windows::Forms::Panel^  binaryThresholdTresholdPanel;
	private: System::Windows::Forms::TableLayoutPanel^  thresholdLayoutPanel;
	private: System::Windows::Forms::Label^  binaryThresholdThresholdLabel;
	private: CustomTrackBar^  binaryThresholdThresholdTrackbar;
	private: System::Windows::Forms::TextBox^  binrayThresholdTresholdtextbox;
	private: System::Windows::Forms::Panel^  negativeFilterPanel;
	private: System::Windows::Forms::Panel^  contourPanel;
	private: System::Windows::Forms::TableLayoutPanel^  contourTableLayout;
	private: System::Windows::Forms::Button^  exportButton;
	private: System::Windows::Forms::WebBrowser^  contourBrowser;
	private: System::Windows::Forms::PictureBox^  maximizeButtonPictureBox;
	private: System::Windows::Forms::Panel^  vectorOptionsPanel;
	private: System::Windows::Forms::Panel^  vectorOptionsbuttonPanel;
	private: System::Windows::Forms::Button^  vectorOptionsButton;
	private: System::Windows::Forms::Panel^  vectorOptionsContainer;
	private: System::Windows::Forms::TableLayoutPanel^  alphaMaxLayoutPanel;
	private: System::Windows::Forms::Label^  alphaMaxLabel;
	private: CustomTrackBar^  alphaMaxTrackBar;
	private: System::Windows::Forms::TextBox^  alphaMaxTextBox;
	private: System::Windows::Forms::Label^  minimizeLabel;
	private: System::Windows::Forms::Panel^  removeSmallContourPanel;
	private: System::Windows::Forms::Panel^  numofObjectsPanel;
	private: System::Windows::Forms::TableLayoutPanel^  numObjectsTableLayoutPanel;
	private: System::Windows::Forms::Label^  numObjectsLabel;
	private: System::Windows::Forms::TextBox^  numOfObjectsTextbox;
	private: System::Windows::Forms::Panel^  removeSmallContourButtonPanel;
	private: CustomCheckboxButton::CustomCheckboxButton^  enableRemoveSmallContourCustomCheckBox;
	private: System::Windows::Forms::TableLayoutPanel^  turdSizeLayoutPanel;
	private: System::Windows::Forms::Label^  turdSizeLabel;
	private: System::Windows::Forms::TextBox^  turdSizeTextBox;
	private: ::CustomCheckboxButton::CustomCheckboxButton^  negativeFilterCheckboxButton;
	private: System::Windows::Forms::Panel^  blurringPanel;
	private: System::Windows::Forms::Panel^  blurringIterationsContainer;
	private: System::Windows::Forms::TableLayoutPanel^  blurringIterationsTableLayout;
	private: System::Windows::Forms::Label^  blurringIterationsLabel;
	private: CustomTrackBar^  blurringTrackBar;
	private: System::Windows::Forms::TextBox^  blurringIterationsTextbox;
	private: System::Windows::Forms::Panel^  blurringCheckBoxButtonPanel;
	private: ::CustomCheckboxButton::CustomCheckboxButton^  blurringCheckBoxButton;
	private: System::Windows::Forms::Panel^  optimizeContoursPanel;
	private: System::Windows::Forms::Panel^  optimizeContoursContainer;
	private: System::Windows::Forms::TableLayoutPanel^  optimizeToleranceTableLayout;
	private: System::Windows::Forms::Label^  label1;
	private: CustomTrackBar^  toleranceTrackBar;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Panel^  optimizeContoursCheckBoxPanel;
	private: ::CustomCheckboxButton::CustomCheckboxButton^  optimizeContoursCheckboxButton;
	private: System::ComponentModel::BackgroundWorker^  backgroundWorker1;
	private: System::Windows::Forms::Panel^  exportBottomPanel;
	private: System::Windows::Forms::Label^  progressBarLabel;
	private: System::Windows::Forms::ProgressBar^  progressBar1;
	private: System::Windows::Forms::Button^  resetROIButton;


	public:
		Main(void)
		{
			Read_Setting_From_XML();

			InitializeComponent();


			if (optimize) {
				optimizeContoursCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkChecked.png");
			}
			else {
				optimizeContoursCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkUnchecked.png");
			}
			optimizeContoursCheckboxButton->IsChecked = optimize;
			this->optimizeContoursContainer->Visible = optimize;

			if (invert) {
				negativeFilterCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkChecked.png");

			}
			else {
				negativeFilterCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkUnchecked.png");
			}
			negativeFilterCheckboxButton->IsChecked = invert;

			if (blurring) {
				blurringCheckBoxButton->Image = Image::FromFile("Icons\\CheckMarkChecked.png");

			}
			else {
				blurringCheckBoxButton->Image = Image::FromFile("Icons\\CheckMarkUnchecked.png");
			}
			blurringCheckBoxButton->IsChecked = blurring;
			this->blurringIterationsContainer->Visible = blurring;
		}



	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Main()
		{
			if (components)
			{
				delete components;
			}
		}




	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Main::typeid));
			this->backgroundWorker1 = (gcnew System::ComponentModel::BackgroundWorker());
			this->topPanel = (gcnew System::Windows::Forms::Panel());
			this->maximizeButtonPictureBox = (gcnew System::Windows::Forms::PictureBox());
			this->minimizeLabel = (gcnew System::Windows::Forms::Label());
			this->closeLabel = (gcnew System::Windows::Forms::Label());
			this->logoPanel = (gcnew System::Windows::Forms::Panel());
			this->leftPanel = (gcnew System::Windows::Forms::Panel());
			this->optionsPanel = (gcnew System::Windows::Forms::Panel());
			this->vectorOptionsPanel = (gcnew System::Windows::Forms::Panel());
			this->vectorOptionsContainer = (gcnew System::Windows::Forms::Panel());
			this->optimizeContoursPanel = (gcnew System::Windows::Forms::Panel());
			this->optimizeContoursContainer = (gcnew System::Windows::Forms::Panel());
			this->optimizeToleranceTableLayout = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->ROITableLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->toleranceTrackBar = (gcnew CustomTrackBar());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->optimizeContoursCheckBoxPanel = (gcnew System::Windows::Forms::Panel());
			this->optimizeContoursCheckboxButton = (gcnew ::CustomCheckboxButton::CustomCheckboxButton());
			this->alphaMaxLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->alphaMaxLabel = (gcnew System::Windows::Forms::Label());
			this->alphaMaxTrackBar = (gcnew CustomTrackBar());
			this->alphaMaxTextBox = (gcnew System::Windows::Forms::TextBox());
			this->vectorOptionsbuttonPanel = (gcnew System::Windows::Forms::Panel());
			this->vectorOptionsButton = (gcnew System::Windows::Forms::Button());
			this->objectsPanel = (gcnew System::Windows::Forms::Panel());
			this->objectsSettingsPanel = (gcnew System::Windows::Forms::Panel());
			this->contourPanel = (gcnew System::Windows::Forms::Panel());
			this->turdSizeLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->turdSizeLabel = (gcnew System::Windows::Forms::Label());
			this->turdSizeTextBox = (gcnew System::Windows::Forms::TextBox());
			this->removeSmallContourPanel = (gcnew System::Windows::Forms::Panel());
			this->numofObjectsPanel = (gcnew System::Windows::Forms::Panel());
			this->numObjectsTableLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->numObjectsLabel = (gcnew System::Windows::Forms::Label());
			this->numOfObjectsTextbox = (gcnew System::Windows::Forms::TextBox());
			this->removeSmallContourButtonPanel = (gcnew System::Windows::Forms::Panel());
			this->enableRemoveSmallContourCustomCheckBox = (gcnew ::CustomCheckboxButton::CustomCheckboxButton());
			this->objectsButtonPanel = (gcnew System::Windows::Forms::Panel());
			this->contourButton = (gcnew System::Windows::Forms::Button());
			this->imageManipulationPanel = (gcnew System::Windows::Forms::Panel());
			this->imageManipulationSettingPanel = (gcnew System::Windows::Forms::Panel());
			this->negativeFilterCheckboxButton = (gcnew ::CustomCheckboxButton::CustomCheckboxButton());
			this->blurringPanel = (gcnew System::Windows::Forms::Panel());
			this->blurringIterationsContainer = (gcnew System::Windows::Forms::Panel());
			this->blurringIterationsTableLayout = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->blurringIterationsLabel = (gcnew System::Windows::Forms::Label());
			this->blurringTrackBar = (gcnew CustomTrackBar());
			this->blurringIterationsTextbox = (gcnew System::Windows::Forms::TextBox());
			this->blurringCheckBoxButtonPanel = (gcnew System::Windows::Forms::Panel());
			this->blurringCheckBoxButton = (gcnew ::CustomCheckboxButton::CustomCheckboxButton());
			this->negativeFilterPanel = (gcnew System::Windows::Forms::Panel());
			this->binaryThresholdSettingsPanel = (gcnew System::Windows::Forms::Panel());
			this->binaryThresholdSettingsContainer = (gcnew System::Windows::Forms::Panel());
			this->binaryThresholdTresholdPanel = (gcnew System::Windows::Forms::Panel());
			this->thresholdLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->binaryThresholdThresholdLabel = (gcnew System::Windows::Forms::Label());
			this->binaryThresholdThresholdTrackbar = (gcnew CustomTrackBar());
			this->binrayThresholdTresholdtextbox = (gcnew System::Windows::Forms::TextBox());
			this->binaryThresholdButtonPanel = (gcnew System::Windows::Forms::Panel());
			this->binaryThresholdSettingsButton = (gcnew System::Windows::Forms::Button());
			this->imageManipulationButtonPanel = (gcnew System::Windows::Forms::Panel());
			this->imageManipulationButton = (gcnew System::Windows::Forms::Button());
			this->ROIPanel = (gcnew System::Windows::Forms::Panel());
			this->findROIButton = (gcnew System::Windows::Forms::Button());
			this->takePicturePanel = (gcnew System::Windows::Forms::Panel());
			this->takePictureButton = (gcnew System::Windows::Forms::Button());
			this->calibrateCameraPanel = (gcnew System::Windows::Forms::Panel());
			this->calibrateCameraButton = (gcnew System::Windows::Forms::Button());
			this->mainTableLayoutPanel = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->contourImagePanel = (gcnew System::Windows::Forms::Panel());
			this->contourTableLayout = (gcnew System::Windows::Forms::TableLayoutPanel());
			this->exportButton = (gcnew System::Windows::Forms::Button());
			this->exportBottomPanel = (gcnew System::Windows::Forms::Panel());
			this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());
			this->progressBarLabel = (gcnew System::Windows::Forms::Label());
			this->contourBrowser = (gcnew System::Windows::Forms::WebBrowser());
			this->contourImageLabel = (gcnew System::Windows::Forms::Label());
			this->originalImagePanel = (gcnew System::Windows::Forms::Panel());
			this->originalImageContainer = (gcnew System::Windows::Forms::Panel());
			this->originalImageBrowser = (gcnew System::Windows::Forms::WebBrowser());
			this->originalImageLabel = (gcnew System::Windows::Forms::Label());
			this->manipulatedImagePanel = (gcnew System::Windows::Forms::Panel());
			this->manipulatedImageContainer = (gcnew System::Windows::Forms::Panel());
			this->manipulatedImageBrowser = (gcnew System::Windows::Forms::WebBrowser());
			this->manipulatedImageLabel = (gcnew System::Windows::Forms::Label());
			this->resetROIButton = (gcnew System::Windows::Forms::Button());

			this->topPanel->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->maximizeButtonPictureBox))->BeginInit();
			this->leftPanel->SuspendLayout();
			this->optionsPanel->SuspendLayout();
			this->vectorOptionsPanel->SuspendLayout();
			this->vectorOptionsContainer->SuspendLayout();
			this->optimizeContoursPanel->SuspendLayout();
			this->optimizeContoursContainer->SuspendLayout();
			this->optimizeToleranceTableLayout->SuspendLayout();
			this->ROITableLayoutPanel->SuspendLayout();
			this->optimizeContoursCheckBoxPanel->SuspendLayout();
			this->alphaMaxLayoutPanel->SuspendLayout();
			this->vectorOptionsbuttonPanel->SuspendLayout();
			this->objectsPanel->SuspendLayout();
			this->objectsSettingsPanel->SuspendLayout();
			this->contourPanel->SuspendLayout();
			this->turdSizeLayoutPanel->SuspendLayout();
			this->removeSmallContourPanel->SuspendLayout();
			this->numofObjectsPanel->SuspendLayout();
			this->numObjectsTableLayoutPanel->SuspendLayout();
			this->removeSmallContourButtonPanel->SuspendLayout();
			this->objectsButtonPanel->SuspendLayout();
			this->imageManipulationPanel->SuspendLayout();
			this->imageManipulationSettingPanel->SuspendLayout();
			this->blurringPanel->SuspendLayout();
			this->blurringIterationsContainer->SuspendLayout();
			this->blurringIterationsTableLayout->SuspendLayout();
			this->blurringCheckBoxButtonPanel->SuspendLayout();
			this->binaryThresholdSettingsPanel->SuspendLayout();
			this->binaryThresholdSettingsContainer->SuspendLayout();
			this->binaryThresholdTresholdPanel->SuspendLayout();
			this->thresholdLayoutPanel->SuspendLayout();
			this->binaryThresholdButtonPanel->SuspendLayout();
			this->imageManipulationButtonPanel->SuspendLayout();
			this->ROIPanel->SuspendLayout();
			this->takePicturePanel->SuspendLayout();
			this->calibrateCameraPanel->SuspendLayout();
			this->mainTableLayoutPanel->SuspendLayout();
			this->contourImagePanel->SuspendLayout();
			this->contourTableLayout->SuspendLayout();
			this->originalImagePanel->SuspendLayout();
			this->originalImageContainer->SuspendLayout();
			this->manipulatedImagePanel->SuspendLayout();
			this->manipulatedImageContainer->SuspendLayout();
			this->SuspendLayout();
			// 
			// backgroundWorker1
			// 
			this->backgroundWorker1->WorkerSupportsCancellation = true;
			this->backgroundWorker1->WorkerReportsProgress = true;
			this->backgroundWorker1->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &Main::backgroundWorker1_DoWork);
			this->backgroundWorker1->RunWorkerCompleted += gcnew RunWorkerCompletedEventHandler(this, &Main::backgroundWorker1_RunWorkerCompleted);
			this->backgroundWorker1->ProgressChanged += gcnew ProgressChangedEventHandler(this, &Main::backgroundWorker1_ProgressChanged);
			// 
			// topPanel
			// 
			this->topPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
				static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->topPanel->Controls->Add(this->maximizeButtonPictureBox);
			this->topPanel->Controls->Add(this->minimizeLabel);
			this->topPanel->Controls->Add(this->closeLabel);
			this->topPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->topPanel->Location = System::Drawing::Point(285, 0);
			this->topPanel->Margin = System::Windows::Forms::Padding(0);
			this->topPanel->Name = L"topPanel";
			this->topPanel->Size = System::Drawing::Size(760, 25);
			this->topPanel->TabIndex = 2;
			this->topPanel->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &Main::topPanel_MouseDown);
			this->topPanel->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &Main::topPanel_MouseMove);
			this->topPanel->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &Main::topPanel_MouseUp);
			// 
			// maximizeButtonPictureBox
			// 
			this->maximizeButtonPictureBox->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->maximizeButtonPictureBox->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"maximizeButtonPictureBox.Image")));
			this->maximizeButtonPictureBox->Location = System::Drawing::Point(707, 9);
			this->maximizeButtonPictureBox->Name = L"maximizeButtonPictureBox";
			this->maximizeButtonPictureBox->Size = System::Drawing::Size(16, 16);
			this->maximizeButtonPictureBox->TabIndex = 3;
			this->maximizeButtonPictureBox->TabStop = false;
			this->maximizeButtonPictureBox->Click += gcnew System::EventHandler(this, &Main::maximizeButtonPictureBox_Click);
			this->maximizeButtonPictureBox->MouseEnter += gcnew System::EventHandler(this, &Main::maximizeButtonPictureBox_MouseEnter);
			this->maximizeButtonPictureBox->MouseLeave += gcnew System::EventHandler(this, &Main::maximizeButtonPictureBox_MouseLeave);
			// 
			// minimizeLabel
			// 
			this->minimizeLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->minimizeLabel->AutoSize = true;
			this->minimizeLabel->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->minimizeLabel->Location = System::Drawing::Point(681, 3);
			this->minimizeLabel->Name = L"minimizeLabel";
			this->minimizeLabel->Size = System::Drawing::Size(17, 18);
			this->minimizeLabel->TabIndex = 1;
			this->minimizeLabel->Text = L"_";
			this->minimizeLabel->Click += gcnew System::EventHandler(this, &Main::minimizeLabel_Click);
			this->minimizeLabel->MouseEnter += gcnew System::EventHandler(this, &Main::label_MouseEnter);
			this->minimizeLabel->MouseLeave += gcnew System::EventHandler(this, &Main::label_MouseLeave);
			// 
			// closeLabel
			// 
			this->closeLabel->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->closeLabel->AutoSize = true;
			this->closeLabel->Font = (gcnew System::Drawing::Font(L"Arial", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->closeLabel->Location = System::Drawing::Point(728, 5);
			this->closeLabel->Name = L"closeLabel";
			this->closeLabel->Size = System::Drawing::Size(19, 18);
			this->closeLabel->TabIndex = 0;
			this->closeLabel->Text = L"X";
			this->closeLabel->Click += gcnew System::EventHandler(this, &Main::label4_Click);
			this->closeLabel->MouseEnter += gcnew System::EventHandler(this, &Main::label_MouseEnter);
			this->closeLabel->MouseLeave += gcnew System::EventHandler(this, &Main::label_MouseLeave);
			// 
			// logoPanel
			// 
			this->logoPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
				static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->logoPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->logoPanel->Location = System::Drawing::Point(0, 0);
			this->logoPanel->Margin = System::Windows::Forms::Padding(0);
			this->logoPanel->Name = L"logoPanel";
			this->logoPanel->Size = System::Drawing::Size(285, 25);
			this->logoPanel->TabIndex = 0;
			this->logoPanel->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &Main::topPanel_MouseDown);
			this->logoPanel->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &Main::logoPanel_MouseMove);
			this->logoPanel->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &Main::topPanel_MouseUp);
			// 
			// leftPanel
			// 
			this->leftPanel->AutoScroll = true;
			this->leftPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
				static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->leftPanel->Controls->Add(this->optionsPanel);
			this->leftPanel->Controls->Add(this->logoPanel);
			this->leftPanel->Dock = System::Windows::Forms::DockStyle::Left;
			this->leftPanel->Location = System::Drawing::Point(0, 0);
			this->leftPanel->Margin = System::Windows::Forms::Padding(0);
			this->leftPanel->Name = L"leftPanel";
			this->leftPanel->Size = System::Drawing::Size(285, 752);
			this->leftPanel->TabIndex = 0;
			// 
			// optionsPanel
			// 
			this->optionsPanel->AutoScroll = true;
			this->optionsPanel->AutoSize = true;
			this->optionsPanel->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->optionsPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
				static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->optionsPanel->Controls->Add(this->calibrateCameraPanel);
			this->optionsPanel->Controls->Add(this->vectorOptionsPanel);
			this->optionsPanel->Controls->Add(this->objectsPanel);
			this->optionsPanel->Controls->Add(this->imageManipulationPanel);
			this->optionsPanel->Controls->Add(this->ROIPanel);
			this->optionsPanel->Controls->Add(this->takePicturePanel);
			this->optionsPanel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->optionsPanel->Location = System::Drawing::Point(0, 38);
			this->optionsPanel->Name = L"optionsPanel";
			this->optionsPanel->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->optionsPanel->Size = System::Drawing::Size(285, 714);
			this->optionsPanel->TabIndex = 6;
			// 
			// vectorOptionsPanel
			// 
			this->vectorOptionsPanel->AutoSize = true;
			this->vectorOptionsPanel->BackColor = System::Drawing::Color::Transparent;
			this->vectorOptionsPanel->Controls->Add(this->vectorOptionsContainer);
			this->vectorOptionsPanel->Controls->Add(this->vectorOptionsbuttonPanel);
			this->vectorOptionsPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->vectorOptionsPanel->Location = System::Drawing::Point(0, 648);
			this->vectorOptionsPanel->Name = L"vectorOptionsPanel";
			this->vectorOptionsPanel->Padding = System::Windows::Forms::Padding(0, 0, 0, 0);
			this->vectorOptionsPanel->Size = System::Drawing::Size(268, 246);
			this->vectorOptionsPanel->TabIndex = 8;
			// 
			// vectorOptionsContainer
			// 
			this->vectorOptionsContainer->AutoSize = true;
			this->vectorOptionsContainer->BackColor = System::Drawing::Color::Transparent;
			this->vectorOptionsContainer->Controls->Add(this->optimizeContoursPanel);
			this->vectorOptionsContainer->Controls->Add(this->alphaMaxLayoutPanel);
			this->vectorOptionsContainer->Dock = System::Windows::Forms::DockStyle::Top;
			this->vectorOptionsContainer->Location = System::Drawing::Point(0, 49);
			this->vectorOptionsContainer->Name = L"vectorOptionsContainer";
			this->vectorOptionsContainer->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
			this->vectorOptionsContainer->Size = System::Drawing::Size(268, 177);
			this->vectorOptionsContainer->TabIndex = 2;
			this->vectorOptionsContainer->Visible = false;
			// 
			// optimizeContoursPanel
			// 
			this->optimizeContoursPanel->AutoSize = true;
			this->optimizeContoursPanel->BackColor = System::Drawing::Color::Transparent;
			this->optimizeContoursPanel->Controls->Add(this->optimizeContoursContainer);
			this->optimizeContoursPanel->Controls->Add(this->optimizeContoursCheckBoxPanel);
			this->optimizeContoursPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->optimizeContoursPanel->Location = System::Drawing::Point(20, 64);
			this->optimizeContoursPanel->Name = L"optimizeContoursPanel";
			this->optimizeContoursPanel->Size = System::Drawing::Size(248, 113);
			this->optimizeContoursPanel->TabIndex = 10;
			// 
			// optimizeContoursContainer
			// 
			this->optimizeContoursContainer->AutoSize = true;
			this->optimizeContoursContainer->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->optimizeContoursContainer->Controls->Add(this->optimizeToleranceTableLayout);
			this->optimizeContoursContainer->Dock = System::Windows::Forms::DockStyle::Top;
			this->optimizeContoursContainer->Location = System::Drawing::Point(0, 49);
			this->optimizeContoursContainer->Name = L"optimizeContoursContainer";
			this->optimizeContoursContainer->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
			this->optimizeContoursContainer->Size = System::Drawing::Size(248, 64);
			this->optimizeContoursContainer->TabIndex = 1;
			this->optimizeContoursContainer->Visible = false;
			// 
			// optimizeToleranceTableLayout
			// 
			this->optimizeToleranceTableLayout->ColumnCount = 2;
			this->optimizeToleranceTableLayout->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				80)));
			this->optimizeToleranceTableLayout->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				20)));
			this->optimizeToleranceTableLayout->Controls->Add(this->label1, 0, 0);
			this->optimizeToleranceTableLayout->Controls->Add(this->toleranceTrackBar, 0, 1);
			this->optimizeToleranceTableLayout->Controls->Add(this->textBox1, 1, 1);
			this->optimizeToleranceTableLayout->Dock = System::Windows::Forms::DockStyle::Top;
			this->optimizeToleranceTableLayout->Location = System::Drawing::Point(20, 0);
			this->optimizeToleranceTableLayout->Name = L"optimizeToleranceTableLayout";
			this->optimizeToleranceTableLayout->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->optimizeToleranceTableLayout->RowCount = 2;
			this->optimizeToleranceTableLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->optimizeToleranceTableLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->optimizeToleranceTableLayout->Size = System::Drawing::Size(228, 64);
			this->optimizeToleranceTableLayout->TabIndex = 7;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
				static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->optimizeToleranceTableLayout->SetColumnSpan(this->label1, 2);
			this->label1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(3, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(222, 32);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Tolerance";
			this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// trackBar2
			// 
			this->toleranceTrackBar->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
				static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->toleranceTrackBar->Dock = System::Windows::Forms::DockStyle::Fill;
			this->toleranceTrackBar->Name = L"toleranceTrackBar";
			this->toleranceTrackBar->NewSize = System::Drawing::Size(176, 26);
			this->toleranceTrackBar->TabIndex = 1;
			this->toleranceTrackBar->Maximum = 100;
			this->toleranceTrackBar->Multiplier = 100;
			this->toleranceTrackBar->ValueChanging += gcnew System::EventHandler(this, &Main::Trackbar_ValueChanging);
			this->toleranceTrackBar->ValueChanged += gcnew System::EventHandler(this, &Main::toleranceTrackbar_ValueChanged);
			this->toleranceTrackBar->Value = tolerance;
			// 
			// textBox1
			// 
			this->textBox1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->textBox1->Location = System::Drawing::Point(185, 35);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(40, 20);
			this->textBox1->TabIndex = 2;
			this->textBox1->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Main::textBox_KeyDown);
			this->textBox1->TextChanged += gcnew System::EventHandler(this, &Main::TextBox_ValueChanged);
			// 
			// optimizeContoursCheckBoxPanel
			// 
			this->optimizeContoursCheckBoxPanel->AutoSize = true;
			this->optimizeContoursCheckBoxPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->optimizeContoursCheckBoxPanel->Controls->Add(this->optimizeContoursCheckboxButton);
			this->optimizeContoursCheckBoxPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->optimizeContoursCheckBoxPanel->Location = System::Drawing::Point(0, 0);
			this->optimizeContoursCheckBoxPanel->Name = L"optimizeContoursCheckBoxPanel";
			this->optimizeContoursCheckBoxPanel->Size = System::Drawing::Size(248, 49);
			this->optimizeContoursCheckBoxPanel->TabIndex = 0;
			// 
			// optimizeContoursCheckboxButton
			// 
			this->optimizeContoursCheckboxButton->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->optimizeContoursCheckboxButton->Dock = System::Windows::Forms::DockStyle::Top;
			this->optimizeContoursCheckboxButton->FlatAppearance->BorderSize = 0;
			this->optimizeContoursCheckboxButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->optimizeContoursCheckboxButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->optimizeContoursCheckboxButton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"optimizeContoursCheckboxButton.Image")));
			this->optimizeContoursCheckboxButton->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->optimizeContoursCheckboxButton->Location = System::Drawing::Point(0, 0);
			this->optimizeContoursCheckboxButton->Name = L"optimizeContoursCheckboxButton";
			this->optimizeContoursCheckboxButton->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->optimizeContoursCheckboxButton->Size = System::Drawing::Size(248, 49);
			this->optimizeContoursCheckboxButton->TabIndex = 8;
			this->optimizeContoursCheckboxButton->Text = L"Optimize Contours";
			this->optimizeContoursCheckboxButton->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->optimizeContoursCheckboxButton->TextImageRelation = System::Windows::Forms::TextImageRelation::ImageBeforeText;
			this->optimizeContoursCheckboxButton->UseVisualStyleBackColor = false;
			this->optimizeContoursCheckboxButton->Click += gcnew System::EventHandler(this, &Main::optimizeContoursCheckboxButton_Click);
			// 
			// alphaMaxLayoutPanel
			// 
			this->alphaMaxLayoutPanel->ColumnCount = 2;
			this->alphaMaxLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				80)));
			this->alphaMaxLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				20)));
			this->alphaMaxLayoutPanel->Controls->Add(this->alphaMaxLabel, 0, 0);
			this->alphaMaxLayoutPanel->Controls->Add(this->alphaMaxTrackBar, 0, 1);
			this->alphaMaxLayoutPanel->Controls->Add(this->alphaMaxTextBox, 1, 1);
			this->alphaMaxLayoutPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->alphaMaxLayoutPanel->Location = System::Drawing::Point(20, 0);
			this->alphaMaxLayoutPanel->Name = L"alphaMaxLayoutPanel";
			this->alphaMaxLayoutPanel->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->alphaMaxLayoutPanel->RowCount = 2;
			this->alphaMaxLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->alphaMaxLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->alphaMaxLayoutPanel->Size = System::Drawing::Size(248, 64);
			this->alphaMaxLayoutPanel->TabIndex = 5;
			// 
			// alphaMaxLabel
			// 
			this->alphaMaxLabel->AutoSize = true;
			this->alphaMaxLabel->BackColor = System::Drawing::Color::Transparent;
			this->alphaMaxLayoutPanel->SetColumnSpan(this->alphaMaxLabel, 2);
			this->alphaMaxLabel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->alphaMaxLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->alphaMaxLabel->Location = System::Drawing::Point(3, 0);
			this->alphaMaxLabel->Name = L"alphaMaxLabel";
			this->alphaMaxLabel->Size = System::Drawing::Size(242, 32);
			this->alphaMaxLabel->TabIndex = 0;
			this->alphaMaxLabel->Text = L"Smoothness:";
			this->alphaMaxLabel->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// alphaMaxTrackBar
			// 
			this->alphaMaxTrackBar->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
				static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->alphaMaxTrackBar->Dock = System::Windows::Forms::DockStyle::Fill;
			this->alphaMaxTrackBar->Maximum = 135;
			this->alphaMaxTrackBar->Name = L"alphaMaxTrackBar";
			this->alphaMaxTrackBar->NewSize = System::Drawing::Size(192, 26);
			this->alphaMaxTrackBar->TabIndex = 1;
			this->alphaMaxTrackBar->Multiplier = 100;
			this->alphaMaxTrackBar->ValueChanging += gcnew System::EventHandler(this, &Main::Trackbar_ValueChanging);
			this->alphaMaxTrackBar->ValueChanged += gcnew System::EventHandler(this, &Main::alphamaxTrackbar_ValueChanged);
			this->alphaMaxTrackBar->Value = alphamax;
			// 
			// alphaMaxTextBox
			// 
			this->alphaMaxTextBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->alphaMaxTextBox->Location = System::Drawing::Point(201, 35);
			this->alphaMaxTextBox->Name = L"alphaMaxTextBox";
			this->alphaMaxTextBox->Size = System::Drawing::Size(44, 20);
			this->alphaMaxTextBox->TabIndex = 2;
			this->alphaMaxTextBox->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Main::textBox_KeyDown);
			this->alphaMaxTextBox->TextChanged += gcnew System::EventHandler(this, &Main::TextBox_ValueChanged);
			// 
			// vectorOptionsbuttonPanel
			// 
			this->vectorOptionsbuttonPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->vectorOptionsbuttonPanel->Controls->Add(this->vectorOptionsButton);
			this->vectorOptionsbuttonPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->vectorOptionsbuttonPanel->Location = System::Drawing::Point(0, 0);
			this->vectorOptionsbuttonPanel->Name = L"vectorOptionsbuttonPanel";
			this->vectorOptionsbuttonPanel->Size = System::Drawing::Size(268, 49);
			this->vectorOptionsbuttonPanel->TabIndex = 1;
			// 
			// vectorOptionsButton
			// 
			this->vectorOptionsButton->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->vectorOptionsButton->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->vectorOptionsButton->Dock = System::Windows::Forms::DockStyle::Fill;
			this->vectorOptionsButton->FlatAppearance->BorderSize = 0;
			this->vectorOptionsButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->vectorOptionsButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Bold,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->vectorOptionsButton->ForeColor = System::Drawing::Color::Black;
			this->vectorOptionsButton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"vectorOptionsButton.Image")));
			this->vectorOptionsButton->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->vectorOptionsButton->Location = System::Drawing::Point(0, 0);
			this->vectorOptionsButton->Name = L"vectorOptionsButton";
			this->vectorOptionsButton->Size = System::Drawing::Size(268, 49);
			this->vectorOptionsButton->TabIndex = 0;
			this->vectorOptionsButton->Text = L"Vector Options";
			this->vectorOptionsButton->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->vectorOptionsButton->TextImageRelation = System::Windows::Forms::TextImageRelation::TextBeforeImage;
			this->vectorOptionsButton->UseVisualStyleBackColor = false;
			this->vectorOptionsButton->Click += gcnew System::EventHandler(this, &Main::OnButtonCollapseClick);
			// 
			// objectsPanel
			// 
			this->objectsPanel->AutoSize = true;
			this->objectsPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->objectsPanel->Controls->Add(this->objectsSettingsPanel);
			this->objectsPanel->Controls->Add(this->objectsButtonPanel);
			this->objectsPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->objectsPanel->Location = System::Drawing::Point(0, 422);
			this->objectsPanel->Name = L"objectsPanel";
			this->objectsPanel->Size = System::Drawing::Size(268, 226);
			this->objectsPanel->TabIndex = 4;
			// 
			// objectsSettingsPanel
			// 
			this->objectsSettingsPanel->AutoSize = true;
			this->objectsSettingsPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->objectsSettingsPanel->Controls->Add(this->contourPanel);
			this->objectsSettingsPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->objectsSettingsPanel->Location = System::Drawing::Point(0, 49);
			this->objectsSettingsPanel->Name = L"objectsSettingsPanel";
			this->objectsSettingsPanel->Size = System::Drawing::Size(268, 177);
			this->objectsSettingsPanel->TabIndex = 1;
			this->objectsSettingsPanel->Visible = false;
			// 
			// contourPanel
			// 
			this->contourPanel->AutoSize = true;
			this->contourPanel->BackColor = System::Drawing::Color::Transparent;
			this->contourPanel->Controls->Add(this->turdSizeLayoutPanel);
			this->contourPanel->Controls->Add(this->removeSmallContourPanel);
			this->contourPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->contourPanel->Location = System::Drawing::Point(0, 0);
			this->contourPanel->Name = L"contourPanel";
			this->contourPanel->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
			this->contourPanel->Size = System::Drawing::Size(268, 177);
			this->contourPanel->TabIndex = 3;
			// 
			// turdSizeLayoutPanel
			// 
			this->turdSizeLayoutPanel->AutoSize = true;
			this->turdSizeLayoutPanel->ColumnCount = 2;
			this->turdSizeLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				80)));
			this->turdSizeLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				20)));
			this->turdSizeLayoutPanel->Controls->Add(this->turdSizeLabel, 0, 0);
			this->turdSizeLayoutPanel->Controls->Add(this->turdSizeTextBox, 1, 0);
			this->turdSizeLayoutPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->turdSizeLayoutPanel->Location = System::Drawing::Point(20, 75);
			this->turdSizeLayoutPanel->Name = L"turdSizeLayoutPanel";
			this->turdSizeLayoutPanel->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->turdSizeLayoutPanel->RowCount = 1;
			this->turdSizeLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->turdSizeLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->turdSizeLayoutPanel->Size = System::Drawing::Size(248, 26);
			this->turdSizeLayoutPanel->TabIndex = 9;
			// 
			// turdSizeLabel
			// 
			this->turdSizeLabel->AutoSize = true;
			this->turdSizeLabel->BackColor = System::Drawing::Color::Transparent;
			this->turdSizeLabel->Dock = System::Windows::Forms::DockStyle::Top;
			this->turdSizeLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->turdSizeLabel->Location = System::Drawing::Point(3, 0);
			this->turdSizeLabel->Name = L"turdSizeLabel";
			this->turdSizeLabel->Size = System::Drawing::Size(192, 18);
			this->turdSizeLabel->TabIndex = 0;
			this->turdSizeLabel->Text = L"Turd Size:";
			this->turdSizeLabel->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// turdSizeTextBox
			// 
			this->turdSizeTextBox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->turdSizeTextBox->Location = System::Drawing::Point(201, 3);
			this->turdSizeTextBox->Name = L"turdSizeTextBox";
			this->turdSizeTextBox->Size = System::Drawing::Size(44, 20);
			this->turdSizeTextBox->TabIndex = 2;
			this->turdSizeTextBox->Text = turdSize.ToString();
			this->turdSizeTextBox->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Main::textBox_KeyDown);
			// 
			// removeSmallContourPanel
			// 
			this->removeSmallContourPanel->AutoSize = true;
			this->removeSmallContourPanel->BackColor = System::Drawing::Color::Chocolate;
			this->removeSmallContourPanel->Controls->Add(this->numofObjectsPanel);
			this->removeSmallContourPanel->Controls->Add(this->removeSmallContourButtonPanel);
			this->removeSmallContourPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->removeSmallContourPanel->Location = System::Drawing::Point(20, 0);
			this->removeSmallContourPanel->Name = L"removeSmallContourPanel";
			this->removeSmallContourPanel->Size = System::Drawing::Size(248, 113);
			this->removeSmallContourPanel->TabIndex = 8;
			// 
			// numofObjectsPanel
			// 
			this->numofObjectsPanel->AutoSize = true;
			this->numofObjectsPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->numofObjectsPanel->Controls->Add(this->numObjectsTableLayoutPanel);
			this->numofObjectsPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->numofObjectsPanel->Location = System::Drawing::Point(0, 49);
			this->numofObjectsPanel->Name = L"numofObjectsPanel";
			this->numofObjectsPanel->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
			this->numofObjectsPanel->Size = System::Drawing::Size(248, 64);
			this->numofObjectsPanel->TabIndex = 1;
			this->numofObjectsPanel->Visible = false;
			// 
			// numObjectsTableLayoutPanel
			// 
			this->numObjectsTableLayoutPanel->AutoSize = true;
			this->numObjectsTableLayoutPanel->ColumnCount = 2;
			this->numObjectsTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				80)));
			this->numObjectsTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				20)));
			this->numObjectsTableLayoutPanel->Controls->Add(this->numObjectsLabel, 0, 0);
			this->numObjectsTableLayoutPanel->Controls->Add(this->numOfObjectsTextbox, 1, 0);
			this->numObjectsTableLayoutPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->numObjectsTableLayoutPanel->Location = System::Drawing::Point(20, 0);
			this->numObjectsTableLayoutPanel->Name = L"numObjectsTableLayoutPanel";
			this->numObjectsTableLayoutPanel->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->numObjectsTableLayoutPanel->RowCount = 1;
			this->numObjectsTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->numObjectsTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->numObjectsTableLayoutPanel->Size = System::Drawing::Size(228, 26);
			this->numObjectsTableLayoutPanel->TabIndex = 7;
			// 
			// numObjectsLabel
			// 
			this->numObjectsLabel->AutoSize = true;
			this->numObjectsLabel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
				static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->numObjectsLabel->Dock = System::Windows::Forms::DockStyle::Top;
			this->numObjectsLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->numObjectsLabel->Location = System::Drawing::Point(3, 0);
			this->numObjectsLabel->Name = L"numObjectsLabel";
			this->numObjectsLabel->Size = System::Drawing::Size(176, 18);
			this->numObjectsLabel->TabIndex = 0;
			this->numObjectsLabel->Text = L"# Objects on Image:";
			this->numObjectsLabel->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// numOfObjectsTextbox
			// 
			this->numOfObjectsTextbox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->numOfObjectsTextbox->Location = System::Drawing::Point(185, 35);
			this->numOfObjectsTextbox->Name = L"numOfObjectsTextbox";
			this->numOfObjectsTextbox->Size = System::Drawing::Size(40, 20);
			this->numOfObjectsTextbox->TabIndex = 2;
			this->numOfObjectsTextbox->Text = numberOfObjects.ToString();
			this->numOfObjectsTextbox->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Main::textBox_KeyDown);
			// 
			// removeSmallContourButtonPanel
			// 
			this->removeSmallContourButtonPanel->AutoSize = true;
			this->removeSmallContourButtonPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->removeSmallContourButtonPanel->Controls->Add(this->enableRemoveSmallContourCustomCheckBox);
			this->removeSmallContourButtonPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->removeSmallContourButtonPanel->Location = System::Drawing::Point(0, 0);
			this->removeSmallContourButtonPanel->Name = L"removeSmallContourButtonPanel";
			this->removeSmallContourButtonPanel->Size = System::Drawing::Size(248, 49);
			this->removeSmallContourButtonPanel->TabIndex = 0;
			// 
			// enableRemoveSmallContourCustomCheckBox
			// 
			this->enableRemoveSmallContourCustomCheckBox->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->enableRemoveSmallContourCustomCheckBox->Dock = System::Windows::Forms::DockStyle::Top;
			this->enableRemoveSmallContourCustomCheckBox->FlatAppearance->BorderSize = 0;
			this->enableRemoveSmallContourCustomCheckBox->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->enableRemoveSmallContourCustomCheckBox->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->enableRemoveSmallContourCustomCheckBox->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"enableRemoveSmallContourCustomCheckBox.Image")));
			this->enableRemoveSmallContourCustomCheckBox->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->enableRemoveSmallContourCustomCheckBox->Location = System::Drawing::Point(0, 0);
			this->enableRemoveSmallContourCustomCheckBox->Name = L"enableRemoveSmallContourCustomCheckBox";
			this->enableRemoveSmallContourCustomCheckBox->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->enableRemoveSmallContourCustomCheckBox->Size = System::Drawing::Size(248, 49);
			this->enableRemoveSmallContourCustomCheckBox->TabIndex = 8;
			this->enableRemoveSmallContourCustomCheckBox->Text = L"Remove Small Contours";
			this->enableRemoveSmallContourCustomCheckBox->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->enableRemoveSmallContourCustomCheckBox->TextImageRelation = System::Windows::Forms::TextImageRelation::ImageBeforeText;
			this->enableRemoveSmallContourCustomCheckBox->UseVisualStyleBackColor = false;
			this->enableRemoveSmallContourCustomCheckBox->Click += gcnew System::EventHandler(this, &Main::enableRemoveSmallContourCustomCheckBox_Click);
			// 
			// objectsButtonPanel
			// 
			this->objectsButtonPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->objectsButtonPanel->Controls->Add(this->contourButton);
			this->objectsButtonPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->objectsButtonPanel->Location = System::Drawing::Point(0, 0);
			this->objectsButtonPanel->Name = L"objectsButtonPanel";
			this->objectsButtonPanel->Size = System::Drawing::Size(268, 49);
			this->objectsButtonPanel->TabIndex = 0;
			// 
			// contourButton
			// 
			this->contourButton->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
				static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->contourButton->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->contourButton->Dock = System::Windows::Forms::DockStyle::Fill;
			this->contourButton->FlatAppearance->BorderSize = 0;
			this->contourButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->contourButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->contourButton->ForeColor = System::Drawing::Color::Black;
			this->contourButton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"contourButton.Image")));
			this->contourButton->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->contourButton->Location = System::Drawing::Point(0, 0);
			this->contourButton->Name = L"contourButton";
			this->contourButton->Size = System::Drawing::Size(268, 49);
			this->contourButton->TabIndex = 0;
			this->contourButton->Text = L"Contours";
			this->contourButton->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->contourButton->TextImageRelation = System::Windows::Forms::TextImageRelation::TextBeforeImage;
			this->contourButton->UseVisualStyleBackColor = false;
			this->contourButton->Click += gcnew System::EventHandler(this, &Main::OnButtonCollapseClick);
			// 
			// imageManipulationPanel
			// 
			this->imageManipulationPanel->AutoSize = true;
			this->imageManipulationPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->imageManipulationPanel->Controls->Add(this->imageManipulationSettingPanel);
			this->imageManipulationPanel->Controls->Add(this->imageManipulationButtonPanel);
			this->imageManipulationPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->imageManipulationPanel->Location = System::Drawing::Point(0, 98);
			this->imageManipulationPanel->Name = L"imageManipulationPanel";
			this->imageManipulationPanel->Size = System::Drawing::Size(268, 324);
			this->imageManipulationPanel->TabIndex = 1;
			// 
			// imageManipulationSettingPanel
			// 
			this->imageManipulationSettingPanel->AutoSize = true;
			this->imageManipulationSettingPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->imageManipulationSettingPanel->Controls->Add(this->negativeFilterCheckboxButton);
			this->imageManipulationSettingPanel->Controls->Add(this->blurringPanel);
			this->imageManipulationSettingPanel->Controls->Add(this->negativeFilterPanel);
			this->imageManipulationSettingPanel->Controls->Add(this->binaryThresholdSettingsPanel);
			this->imageManipulationSettingPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->imageManipulationSettingPanel->Location = System::Drawing::Point(0, 49);
			this->imageManipulationSettingPanel->Name = L"imageManipulationSettingPanel";
			this->imageManipulationSettingPanel->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
			this->imageManipulationSettingPanel->Size = System::Drawing::Size(268, 275);
			this->imageManipulationSettingPanel->TabIndex = 1;
			this->imageManipulationSettingPanel->Visible = false;
			// 
			// negativeFilterCheckboxButton
			// 
			this->negativeFilterCheckboxButton->Dock = System::Windows::Forms::DockStyle::Top;
			this->negativeFilterCheckboxButton->FlatAppearance->BorderSize = 0;
			this->negativeFilterCheckboxButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->negativeFilterCheckboxButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->negativeFilterCheckboxButton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"negativeFilterCheckboxButton.Image")));
			this->negativeFilterCheckboxButton->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->negativeFilterCheckboxButton->Location = System::Drawing::Point(20, 226);
			this->negativeFilterCheckboxButton->Name = L"negativeFilterCheckboxButton";
			this->negativeFilterCheckboxButton->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->negativeFilterCheckboxButton->Size = System::Drawing::Size(248, 49);
			this->negativeFilterCheckboxButton->TabIndex = 10;
			this->negativeFilterCheckboxButton->Text = L"Invert Color";
			this->negativeFilterCheckboxButton->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->negativeFilterCheckboxButton->TextImageRelation = System::Windows::Forms::TextImageRelation::ImageBeforeText;
			this->negativeFilterCheckboxButton->UseVisualStyleBackColor = true;
			this->negativeFilterCheckboxButton->Click += gcnew System::EventHandler(this, &Main::negativeFilterCheckboxButton_Click);
			// 
			// blurringPanel
			// 
			this->blurringPanel->AutoSize = true;
			this->blurringPanel->BackColor = System::Drawing::Color::Chocolate;
			this->blurringPanel->Controls->Add(this->blurringIterationsContainer);
			this->blurringPanel->Controls->Add(this->blurringCheckBoxButtonPanel);
			this->blurringPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->blurringPanel->Location = System::Drawing::Point(20, 113);
			this->blurringPanel->Name = L"blurringPanel";
			this->blurringPanel->Size = System::Drawing::Size(248, 113);
			this->blurringPanel->TabIndex = 9;
			// 
			// blurringIterationsContainer
			// 
			this->blurringIterationsContainer->AutoSize = true;
			this->blurringIterationsContainer->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->blurringIterationsContainer->Controls->Add(this->blurringIterationsTableLayout);
			this->blurringIterationsContainer->Dock = System::Windows::Forms::DockStyle::Top;
			this->blurringIterationsContainer->Location = System::Drawing::Point(0, 49);
			this->blurringIterationsContainer->Name = L"blurringIterationsContainer";
			this->blurringIterationsContainer->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
			this->blurringIterationsContainer->Size = System::Drawing::Size(248, 64);
			this->blurringIterationsContainer->TabIndex = 1;
			this->blurringIterationsContainer->Visible = false;
			// 
			// blurringIterationsTableLayout
			// 
			this->blurringIterationsTableLayout->ColumnCount = 2;
			this->blurringIterationsTableLayout->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				80)));
			this->blurringIterationsTableLayout->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				20)));
			this->blurringIterationsTableLayout->Controls->Add(this->blurringIterationsLabel, 0, 0);
			this->blurringIterationsTableLayout->Controls->Add(this->blurringTrackBar, 0, 1);
			this->blurringIterationsTableLayout->Controls->Add(this->blurringIterationsTextbox, 1, 1);
			this->blurringIterationsTableLayout->Dock = System::Windows::Forms::DockStyle::Top;
			this->blurringIterationsTableLayout->Location = System::Drawing::Point(20, 0);
			this->blurringIterationsTableLayout->Name = L"blurringIterationsTableLayout";
			this->blurringIterationsTableLayout->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->blurringIterationsTableLayout->RowCount = 2;
			this->blurringIterationsTableLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->blurringIterationsTableLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->blurringIterationsTableLayout->Size = System::Drawing::Size(228, 64);
			this->blurringIterationsTableLayout->TabIndex = 7;
			// 
			// blurringIterationsLabel
			// 
			this->blurringIterationsLabel->AutoSize = true;
			this->blurringIterationsLabel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->blurringIterationsTableLayout->SetColumnSpan(this->blurringIterationsLabel, 2);
			this->blurringIterationsLabel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->blurringIterationsLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->blurringIterationsLabel->Location = System::Drawing::Point(3, 0);
			this->blurringIterationsLabel->Name = L"blurringIterationsLabel";
			this->blurringIterationsLabel->Size = System::Drawing::Size(222, 32);
			this->blurringIterationsLabel->TabIndex = 0;
			this->blurringIterationsLabel->Text = L"Blurring Iterations";
			this->blurringIterationsLabel->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// trackBar1
			// 
			this->blurringTrackBar->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
				static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->blurringTrackBar->Dock = System::Windows::Forms::DockStyle::Fill;
			this->blurringTrackBar->Maximum = 20;
			this->blurringTrackBar->Name = L"blurringTrackBar";
			this->blurringTrackBar->NewSize = System::Drawing::Size(176, 26);
			this->blurringTrackBar->TabIndex = 1;
			this->blurringTrackBar->ValueChanging += gcnew System::EventHandler(this, &Main::Trackbar_ValueChanging);
			this->blurringTrackBar->ValueChanged += gcnew System::EventHandler(this, &Main::blurringTrackbar_ValueChanged);
			this->blurringTrackBar->Value = this->morph_iterations;
			// 
			// blurringIterationsTextbox
			// 
			this->blurringIterationsTextbox->CharacterCasing = System::Windows::Forms::CharacterCasing::Upper;
			this->blurringIterationsTextbox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->blurringIterationsTextbox->Location = System::Drawing::Point(185, 35);
			this->blurringIterationsTextbox->Name = L"blurringIterationsTextbox";
			this->blurringIterationsTextbox->Size = System::Drawing::Size(40, 20);
			this->blurringIterationsTextbox->TabIndex = 2;
			this->blurringIterationsTextbox->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Main::textBox_KeyDown);
			this->blurringIterationsTextbox->TextChanged += gcnew System::EventHandler(this, &Main::TextBox_ValueChanged);
			// 
			// blurringCheckBoxButtonPanel
			// 
			this->blurringCheckBoxButtonPanel->AutoSize = true;
			this->blurringCheckBoxButtonPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->blurringCheckBoxButtonPanel->Controls->Add(this->blurringCheckBoxButton);
			this->blurringCheckBoxButtonPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->blurringCheckBoxButtonPanel->Location = System::Drawing::Point(0, 0);
			this->blurringCheckBoxButtonPanel->Name = L"blurringCheckBoxButtonPanel";
			this->blurringCheckBoxButtonPanel->Size = System::Drawing::Size(248, 49);
			this->blurringCheckBoxButtonPanel->TabIndex = 0;
			// 
			// blurringCheckBoxButton
			// 
			this->blurringCheckBoxButton->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->blurringCheckBoxButton->Dock = System::Windows::Forms::DockStyle::Top;
			this->blurringCheckBoxButton->FlatAppearance->BorderSize = 0;
			this->blurringCheckBoxButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->blurringCheckBoxButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->blurringCheckBoxButton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"blurringCheckBoxButton.Image")));
			this->blurringCheckBoxButton->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->blurringCheckBoxButton->Location = System::Drawing::Point(0, 0);
			this->blurringCheckBoxButton->Name = L"blurringCheckBoxButton";
			this->blurringCheckBoxButton->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->blurringCheckBoxButton->Size = System::Drawing::Size(248, 49);
			this->blurringCheckBoxButton->TabIndex = 8;
			this->blurringCheckBoxButton->Text = L"Blurring";
			this->blurringCheckBoxButton->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->blurringCheckBoxButton->TextImageRelation = System::Windows::Forms::TextImageRelation::ImageBeforeText;
			this->blurringCheckBoxButton->UseVisualStyleBackColor = false;
			this->blurringCheckBoxButton->Click += gcnew System::EventHandler(this, &Main::blurringCheckBoxButton_Click);
			// 
			// negativeFilterPanel
			// 
			this->negativeFilterPanel->AutoSize = true;
			this->negativeFilterPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->negativeFilterPanel->Location = System::Drawing::Point(20, 113);
			this->negativeFilterPanel->Name = L"negativeFilterPanel";
			this->negativeFilterPanel->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
			this->negativeFilterPanel->Size = System::Drawing::Size(248, 0);
			this->negativeFilterPanel->TabIndex = 4;
			// 
			// binaryThresholdSettingsPanel
			// 
			this->binaryThresholdSettingsPanel->AutoSize = true;
			this->binaryThresholdSettingsPanel->BackColor = System::Drawing::Color::Transparent;
			this->binaryThresholdSettingsPanel->Controls->Add(this->binaryThresholdSettingsContainer);
			this->binaryThresholdSettingsPanel->Controls->Add(this->binaryThresholdButtonPanel);
			this->binaryThresholdSettingsPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->binaryThresholdSettingsPanel->Location = System::Drawing::Point(20, 0);
			this->binaryThresholdSettingsPanel->Name = L"binaryThresholdSettingsPanel";
			this->binaryThresholdSettingsPanel->Size = System::Drawing::Size(248, 113);
			this->binaryThresholdSettingsPanel->TabIndex = 2;
			// 
			// binaryThresholdSettingsContainer
			// 
			this->binaryThresholdSettingsContainer->AutoSize = true;
			this->binaryThresholdSettingsContainer->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->binaryThresholdSettingsContainer->Controls->Add(this->binaryThresholdTresholdPanel);
			this->binaryThresholdSettingsContainer->Dock = System::Windows::Forms::DockStyle::Top;
			this->binaryThresholdSettingsContainer->Location = System::Drawing::Point(0, 49);
			this->binaryThresholdSettingsContainer->Name = L"binaryThresholdSettingsContainer";
			this->binaryThresholdSettingsContainer->Padding = System::Windows::Forms::Padding(20, 0, 0, 0);
			this->binaryThresholdSettingsContainer->Size = System::Drawing::Size(248, 64);
			this->binaryThresholdSettingsContainer->TabIndex = 1;
			this->binaryThresholdSettingsContainer->Visible = false;
			// 
			// binaryThresholdTresholdPanel
			// 
			this->binaryThresholdTresholdPanel->BackColor = System::Drawing::Color::Transparent;
			this->binaryThresholdTresholdPanel->Controls->Add(this->thresholdLayoutPanel);
			this->binaryThresholdTresholdPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->binaryThresholdTresholdPanel->Location = System::Drawing::Point(20, 0);
			this->binaryThresholdTresholdPanel->Name = L"binaryThresholdTresholdPanel";
			this->binaryThresholdTresholdPanel->Size = System::Drawing::Size(228, 64);
			this->binaryThresholdTresholdPanel->TabIndex = 0;
			// 
			// thresholdLayoutPanel
			// 
			this->thresholdLayoutPanel->ColumnCount = 2;
			this->thresholdLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				80)));
			this->thresholdLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				20)));
			this->thresholdLayoutPanel->Controls->Add(this->binaryThresholdThresholdLabel, 0, 0);
			this->thresholdLayoutPanel->Controls->Add(this->binaryThresholdThresholdTrackbar, 0, 1);
			this->thresholdLayoutPanel->Controls->Add(this->binrayThresholdTresholdtextbox, 1, 1);
			this->thresholdLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->thresholdLayoutPanel->Location = System::Drawing::Point(0, 0);
			this->thresholdLayoutPanel->Name = L"thresholdLayoutPanel";
			this->thresholdLayoutPanel->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->thresholdLayoutPanel->RowCount = 2;
			this->thresholdLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->thresholdLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->thresholdLayoutPanel->Size = System::Drawing::Size(228, 64);
			this->thresholdLayoutPanel->TabIndex = 0;
			// 
			// binaryThresholdThresholdLabel
			// 
			this->binaryThresholdThresholdLabel->AutoSize = true;
			this->binaryThresholdThresholdLabel->BackColor = System::Drawing::Color::Transparent;
			this->thresholdLayoutPanel->SetColumnSpan(this->binaryThresholdThresholdLabel, 2);
			this->binaryThresholdThresholdLabel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->binaryThresholdThresholdLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->binaryThresholdThresholdLabel->Location = System::Drawing::Point(3, 0);
			this->binaryThresholdThresholdLabel->Name = L"binaryThresholdThresholdLabel";
			this->binaryThresholdThresholdLabel->Size = System::Drawing::Size(222, 32);
			this->binaryThresholdThresholdLabel->TabIndex = 0;
			this->binaryThresholdThresholdLabel->Text = L"Threshold:";
			this->binaryThresholdThresholdLabel->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// binaryThresholdThresholdTrackbar
			// 
			this->binaryThresholdThresholdTrackbar->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->binaryThresholdThresholdTrackbar->Dock = System::Windows::Forms::DockStyle::Fill;
			this->binaryThresholdThresholdTrackbar->Maximum = 255;
			this->binaryThresholdThresholdTrackbar->Name = L"binaryThresholdThresholdTrackbar";
			this->binaryThresholdThresholdTrackbar->NewSize = System::Drawing::Size(176, 26);
			this->binaryThresholdThresholdTrackbar->ValueChanging += gcnew System::EventHandler(this, &Main::Trackbar_ValueChanging);
			this->binaryThresholdThresholdTrackbar->ValueChanged += gcnew System::EventHandler(this, &Main::binaryThresholdThresholdTrackbar_ValueChanged);
			this->binaryThresholdThresholdTrackbar->Value = this->binary_thres;
			// 
			// binrayThresholdTresholdtextbox
			// 
			this->binrayThresholdTresholdtextbox->Dock = System::Windows::Forms::DockStyle::Fill;
			this->binrayThresholdTresholdtextbox->Location = System::Drawing::Point(185, 35);
			this->binrayThresholdTresholdtextbox->Name = L"binrayThresholdTresholdtextbox";
			this->binrayThresholdTresholdtextbox->Size = System::Drawing::Size(40, 20);
			this->binrayThresholdTresholdtextbox->TabIndex = 2;
			this->binrayThresholdTresholdtextbox->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Main::textBox_KeyDown);
			this->binrayThresholdTresholdtextbox->TextChanged += gcnew System::EventHandler(this, &Main::TextBox_ValueChanged);
			// 
			// binaryThresholdButtonPanel
			// 
			this->binaryThresholdButtonPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->binaryThresholdButtonPanel->Controls->Add(this->binaryThresholdSettingsButton);
			this->binaryThresholdButtonPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->binaryThresholdButtonPanel->Location = System::Drawing::Point(0, 0);
			this->binaryThresholdButtonPanel->Name = L"binaryThresholdButtonPanel";
			this->binaryThresholdButtonPanel->Size = System::Drawing::Size(248, 49);
			this->binaryThresholdButtonPanel->TabIndex = 0;
			// 
			// binaryThresholdSettingsButton
			// 
			this->binaryThresholdSettingsButton->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->binaryThresholdSettingsButton->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->binaryThresholdSettingsButton->Dock = System::Windows::Forms::DockStyle::Fill;
			this->binaryThresholdSettingsButton->FlatAppearance->BorderSize = 0;
			this->binaryThresholdSettingsButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->binaryThresholdSettingsButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->binaryThresholdSettingsButton->ForeColor = System::Drawing::Color::Black;
			this->binaryThresholdSettingsButton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"binaryThresholdSettingsButton.Image")));
			this->binaryThresholdSettingsButton->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->binaryThresholdSettingsButton->Location = System::Drawing::Point(0, 0);
			this->binaryThresholdSettingsButton->Name = L"binaryThresholdSettingsButton";
			this->binaryThresholdSettingsButton->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->binaryThresholdSettingsButton->Size = System::Drawing::Size(248, 49);
			this->binaryThresholdSettingsButton->TabIndex = 0;
			this->binaryThresholdSettingsButton->Text = L"Binary Threshold";
			this->binaryThresholdSettingsButton->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->binaryThresholdSettingsButton->TextImageRelation = System::Windows::Forms::TextImageRelation::ImageBeforeText;
			this->binaryThresholdSettingsButton->UseVisualStyleBackColor = false;
			this->binaryThresholdSettingsButton->Click += gcnew System::EventHandler(this, &Main::OnButtonCollapseClick);
			// 
			// imageManipulationButtonPanel
			// 
			this->imageManipulationButtonPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->imageManipulationButtonPanel->Controls->Add(this->imageManipulationButton);
			this->imageManipulationButtonPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->imageManipulationButtonPanel->Location = System::Drawing::Point(0, 0);
			this->imageManipulationButtonPanel->Name = L"imageManipulationButtonPanel";
			this->imageManipulationButtonPanel->Size = System::Drawing::Size(268, 49);
			this->imageManipulationButtonPanel->TabIndex = 0;
			// 
			// imageManipulationButton
			// 
			this->imageManipulationButton->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->imageManipulationButton->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->imageManipulationButton->Dock = System::Windows::Forms::DockStyle::Fill;
			this->imageManipulationButton->FlatAppearance->BorderSize = 0;
			this->imageManipulationButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->imageManipulationButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Bold,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->imageManipulationButton->ForeColor = System::Drawing::Color::Black;
			this->imageManipulationButton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"imageManipulationButton.Image")));
			this->imageManipulationButton->ImageAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->imageManipulationButton->Location = System::Drawing::Point(0, 0);
			this->imageManipulationButton->Name = L"imageManipulationButton";
			this->imageManipulationButton->Size = System::Drawing::Size(268, 49);
			this->imageManipulationButton->TabIndex = 0;
			this->imageManipulationButton->Text = L"Image Manipulation";
			this->imageManipulationButton->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->imageManipulationButton->TextImageRelation = System::Windows::Forms::TextImageRelation::TextBeforeImage;
			this->imageManipulationButton->UseVisualStyleBackColor = false;
			this->imageManipulationButton->Click += gcnew System::EventHandler(this, &Main::OnButtonCollapseClick);
			// 
			// ROITableLayoutPanel
			// 
			this->ROITableLayoutPanel->ColumnCount = 2;
			this->ROITableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				85.71429F)));
			this->ROITableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				14.28571F)));
			this->ROITableLayoutPanel->Controls->Add(this->findROIButton, 0, 0);
			this->ROITableLayoutPanel->Controls->Add(this->resetROIButton, 1, 0);
			this->ROITableLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->ROITableLayoutPanel->Location = System::Drawing::Point(0, 0);
			this->ROITableLayoutPanel->Name = L"ROITableLayoutPanel";
			this->ROITableLayoutPanel->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->ROITableLayoutPanel->RowCount = 1;
			this->ROITableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				100)));
			this->ROITableLayoutPanel->Size = System::Drawing::Size(268, 49);
			this->ROITableLayoutPanel->TabIndex = 0;
			this->ROITableLayoutPanel->AutoSize = true;
			// 
			// ROIPanel
			// 
			this->ROIPanel->AutoSize = true;
			this->ROIPanel->Controls->Add(this->ROITableLayoutPanel);
			this->ROIPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->ROIPanel->Location = System::Drawing::Point(0, 49);
			this->ROIPanel->Name = L"ROIPanel";
			this->ROIPanel->Size = System::Drawing::Size(268, 49);
			this->ROIPanel->TabIndex = 5;
			// 
			// findROIButton
			// 
			this->findROIButton->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
				static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->findROIButton->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->findROIButton->Dock = System::Windows::Forms::DockStyle::Top;
			this->findROIButton->FlatAppearance->BorderSize = 0;
			this->findROIButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->findROIButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.0F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->findROIButton->ForeColor = System::Drawing::Color::Black;
			this->findROIButton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"findROIButton.Image")));
			this->findROIButton->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->findROIButton->Location = System::Drawing::Point(0, 0);
			this->findROIButton->Name = L"findROIButton";
			this->findROIButton->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->findROIButton->Size = System::Drawing::Size(268, 49);
			this->findROIButton->TabIndex = 1;
			this->findROIButton->Text = L"Region of Interest";
			this->findROIButton->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->findROIButton->TextImageRelation = System::Windows::Forms::TextImageRelation::ImageBeforeText;
			this->findROIButton->UseVisualStyleBackColor = false;
			this->findROIButton->Click += gcnew System::EventHandler(this, &Main::ROI_Click);
			// 
			// resetROIButton
			// 
			this->resetROIButton->Dock = System::Windows::Forms::DockStyle::Fill;
			this->resetROIButton->FlatAppearance->BorderSize = 0;
			this->resetROIButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->resetROIButton->ForeColor = System::Drawing::SystemColors::ControlText;
			this->resetROIButton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resetROIButton.Image")));
			this->resetROIButton->Location = System::Drawing::Point(232, 3);
			this->resetROIButton->Name = L"resetROIButton";
			this->resetROIButton->Size = System::Drawing::Size(33, 43);
			this->resetROIButton->TabIndex = 3;
			this->resetROIButton->UseVisualStyleBackColor = true;
			this->resetROIButton->Click += gcnew System::EventHandler(this, &Main::ROIReset_Click);
			// 
			// takePicturePanel
			// 
			this->takePicturePanel->AutoSize = true;
			this->takePicturePanel->Controls->Add(this->takePictureButton);
			this->takePicturePanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->takePicturePanel->Location = System::Drawing::Point(0, 0);
			this->takePicturePanel->Name = L"takePicturePanel";
			this->takePicturePanel->Size = System::Drawing::Size(268, 49);
			this->takePicturePanel->TabIndex = 7;
			// 
			// takePictureButton
			// 
			this->takePictureButton->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->takePictureButton->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->takePictureButton->Dock = System::Windows::Forms::DockStyle::Top;
			this->takePictureButton->FlatAppearance->BorderSize = 0;
			this->takePictureButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->takePictureButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->takePictureButton->ForeColor = System::Drawing::Color::Black;
			this->takePictureButton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"takePictureButton.Image")));
			this->takePictureButton->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->takePictureButton->Location = System::Drawing::Point(0, 0);
			this->takePictureButton->Name = L"takePictureButton";
			this->takePictureButton->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->takePictureButton->Size = System::Drawing::Size(268, 49);
			this->takePictureButton->TabIndex = 1;
			this->takePictureButton->Text = L"Take Picture";
			this->takePictureButton->TextImageRelation = System::Windows::Forms::TextImageRelation::ImageBeforeText;
			this->takePictureButton->UseVisualStyleBackColor = false;
			this->takePictureButton->Click += gcnew System::EventHandler(this, &Main::takePictureButton_Click);
			// 
			// calibrateCameraPanel
			// 
			this->calibrateCameraPanel->AutoSize = true;
			this->calibrateCameraPanel->Controls->Add(this->calibrateCameraButton);
			this->calibrateCameraPanel->Dock = System::Windows::Forms::DockStyle::Top;
			this->calibrateCameraPanel->Location = System::Drawing::Point(0, 0);
			this->calibrateCameraPanel->Name = L"calibrateCameraPanel";
			this->calibrateCameraPanel->Size = System::Drawing::Size(268, 49);
			this->calibrateCameraPanel->TabIndex = 7;
			this->calibrateCameraPanel->Padding = System::Windows::Forms::Padding(0, 0, 0, 20);
			// 
			// calibrateCameraButton
			// 
			this->calibrateCameraButton->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)),
				static_cast<System::Int32>(static_cast<System::Byte>(97)), static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->calibrateCameraButton->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->calibrateCameraButton->Dock = System::Windows::Forms::DockStyle::Top;
			this->calibrateCameraButton->FlatAppearance->BorderSize = 0;
			this->calibrateCameraButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->calibrateCameraButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->calibrateCameraButton->ForeColor = System::Drawing::Color::Black;
			this->calibrateCameraButton->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"calibrateCamera.Image")));
			this->calibrateCameraButton->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->calibrateCameraButton->Location = System::Drawing::Point(0, 0);
			this->calibrateCameraButton->Name = L"calibrateCameraButton";
			this->calibrateCameraButton->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->calibrateCameraButton->Size = System::Drawing::Size(268, 49);
			this->calibrateCameraButton->TabIndex = 1;
			this->calibrateCameraButton->Text = L"Calibrate Camera";
			this->calibrateCameraButton->TextImageRelation = System::Windows::Forms::TextImageRelation::ImageBeforeText;
			this->calibrateCameraButton->UseVisualStyleBackColor = false;
			this->calibrateCameraButton->Click += gcnew System::EventHandler(this, &Main::calibrateCameraButton_Click);
			
			// 
			// mainTableLayoutPanel
			// 
			this->mainTableLayoutPanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(139)), static_cast<System::Int32>(static_cast<System::Byte>(159)),
				static_cast<System::Int32>(static_cast<System::Byte>(183)));
			this->mainTableLayoutPanel->ColumnCount = 2;
			this->mainTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->mainTableLayoutPanel->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent,
				50)));
			this->mainTableLayoutPanel->Controls->Add(this->contourImagePanel, 0, 1);
			this->mainTableLayoutPanel->Controls->Add(this->originalImagePanel, 0, 0);
			this->mainTableLayoutPanel->Controls->Add(this->manipulatedImagePanel, 1, 0);
			this->mainTableLayoutPanel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->mainTableLayoutPanel->Location = System::Drawing::Point(285, 38);
			this->mainTableLayoutPanel->Name = L"mainTableLayoutPanel";
			this->mainTableLayoutPanel->RowCount = 2;
			this->mainTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				37.69471F)));
			this->mainTableLayoutPanel->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent,
				62.30529F)));
			this->mainTableLayoutPanel->Size = System::Drawing::Size(760, 714);
			this->mainTableLayoutPanel->TabIndex = 3;
			// 
			// contourImagePanel
			// 
			this->contourImagePanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(139)), static_cast<System::Int32>(static_cast<System::Byte>(159)),
				static_cast<System::Int32>(static_cast<System::Byte>(183)));
			this->mainTableLayoutPanel->SetColumnSpan(this->contourImagePanel, 2);
			this->contourImagePanel->Controls->Add(this->contourTableLayout);
			this->contourImagePanel->Controls->Add(this->contourImageLabel);
			this->contourImagePanel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->contourImagePanel->Location = System::Drawing::Point(3, 272);
			this->contourImagePanel->Name = L"contourImagePanel";
			this->contourImagePanel->Size = System::Drawing::Size(754, 439);
			this->contourImagePanel->TabIndex = 0;
			// 
			// contourTableLayout
			// 
			this->contourTableLayout->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(139)),
				static_cast<System::Int32>(static_cast<System::Byte>(159)), static_cast<System::Int32>(static_cast<System::Byte>(183)));
			this->contourTableLayout->ColumnCount = 1;
			this->contourTableLayout->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->contourTableLayout->Controls->Add(this->contourBrowser, 0, 0);
			this->contourTableLayout->Controls->Add(this->exportBottomPanel, 0, 2);
			this->contourTableLayout->Dock = System::Windows::Forms::DockStyle::Fill;
			this->contourTableLayout->Location = System::Drawing::Point(0, 41);
			this->contourTableLayout->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->contourTableLayout->Name = L"contourTableLayout";
			this->contourTableLayout->RowCount = 3;
			this->contourTableLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 100)));
			this->contourTableLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle()));
			this->contourTableLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Absolute, 40)));
			this->contourTableLayout->Size = System::Drawing::Size(1005, 499);
			this->contourTableLayout->TabIndex = 2;
			this->contourBrowser->IsWebBrowserContextMenuEnabled = false;
			// 
			// exportBottomPanel
			// 
			this->exportBottomPanel->Controls->Add(this->progressBarLabel);
			this->exportBottomPanel->Controls->Add(this->progressBar1);
			this->exportBottomPanel->Controls->Add(this->exportButton);
			this->exportBottomPanel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->exportBottomPanel->Location = System::Drawing::Point(3, 441);
			this->exportBottomPanel->Name = L"exportBottomPanel";
			this->exportBottomPanel->Size = System::Drawing::Size(999, 40);
			this->exportBottomPanel->TabIndex = 2;
			// 
			// exportButton
			// 
			this->exportButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->exportButton->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
				static_cast<System::Int32>(static_cast<System::Byte>(125)));
			this->exportButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->exportButton->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->exportButton->Location = System::Drawing::Point(864, 8);
			this->exportButton->Margin = System::Windows::Forms::Padding(4);
			this->exportButton->Name = L"exportButton";
			this->exportButton->Size = System::Drawing::Size(123, 30);
			this->exportButton->TabIndex = 0;
			this->exportButton->Text = L"Export";
			this->exportButton->UseVisualStyleBackColor = false;
			this->exportButton->Click += gcnew System::EventHandler(this, &Main::exportButton_Click);
			// 
			// progressBar1
			// 
			this->progressBar1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->progressBar1->Location = System::Drawing::Point(3, 11);
			this->progressBar1->Name = L"progressBar1";
			this->progressBar1->Size = System::Drawing::Size(205, 25);
			this->progressBar1->TabIndex = 1;
			this->progressBar1->Visible = false;
			// 
			// progressBarLabel
			// 
			this->progressBarLabel->AutoSize = true;
			this->progressBarLabel->Location = System::Drawing::Point(224, 12);
			this->progressBarLabel->Name = L"progressBarLabel";
			this->progressBarLabel->Size = System::Drawing::Size(46, 17);
			this->progressBarLabel->TabIndex = 2;
			this->progressBarLabel->Text = L"";
			this->progressBarLabel->Visible = false;
			// 
			// contourBrowser
			// 
			this->contourBrowser->Dock = System::Windows::Forms::DockStyle::Fill;
			this->contourBrowser->Location = System::Drawing::Point(3, 3);
			this->contourBrowser->MinimumSize = System::Drawing::Size(20, 20);
			this->contourBrowser->Name = L"contourBrowser";
			this->contourBrowser->Size = System::Drawing::Size(748, 364);
			this->contourBrowser->TabIndex = 1;
			this->contourBrowser->IsWebBrowserContextMenuEnabled = false;
			this->contourBrowser->ScrollBarsEnabled = false;
			// 
			// contourImageLabel
			// 
			this->contourImageLabel->Dock = System::Windows::Forms::DockStyle::Top;
			this->contourImageLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->contourImageLabel->Location = System::Drawing::Point(0, 0);
			this->contourImageLabel->Name = L"contourImageLabel";
			this->contourImageLabel->Size = System::Drawing::Size(754, 33);
			this->contourImageLabel->TabIndex = 1;
			this->contourImageLabel->Text = L"Vectorized Contours";
			this->contourImageLabel->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// originalImagePanel
			// 
			this->originalImagePanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(139)), static_cast<System::Int32>(static_cast<System::Byte>(159)),
				static_cast<System::Int32>(static_cast<System::Byte>(183)));
			this->originalImagePanel->Controls->Add(this->originalImageContainer);
			this->originalImagePanel->Controls->Add(this->originalImageLabel);
			this->originalImagePanel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->originalImagePanel->ForeColor = System::Drawing::Color::Black;
			this->originalImagePanel->Location = System::Drawing::Point(3, 3);
			this->originalImagePanel->Name = L"originalImagePanel";
			this->originalImagePanel->Size = System::Drawing::Size(374, 263);
			this->originalImagePanel->TabIndex = 1;
			// 
			// originalImageContainer
			// 
			this->originalImageContainer->AutoScroll = true;
			this->originalImageContainer->Controls->Add(this->originalImageBrowser);
			this->originalImageContainer->Dock = System::Windows::Forms::DockStyle::Fill;
			this->originalImageContainer->Location = System::Drawing::Point(0, 33);
			this->originalImageContainer->Name = L"originalImageContainer";
			this->originalImageContainer->Size = System::Drawing::Size(374, 230);
			this->originalImageContainer->TabIndex = 1;
			// 
			// originalImagePictureBox
			// 
			this->originalImageBrowser->BackColor = System::Drawing::Color::Aqua;
			this->originalImageBrowser->Dock = System::Windows::Forms::DockStyle::Fill;
			this->originalImageBrowser->Location = System::Drawing::Point(3, 3);
			this->originalImageBrowser->MinimumSize = System::Drawing::Size(20, 20);
			this->originalImageBrowser->Name = L"originalImageBrowser";
			this->originalImageBrowser->Size = System::Drawing::Size(748, 364);
			this->originalImageBrowser->TabIndex = 1;
			this->originalImageBrowser->ScrollBarsEnabled = false;
			this->originalImageBrowser->IsWebBrowserContextMenuEnabled = false;
			// 
			// originalImageLabel
			// 
			this->originalImageLabel->Dock = System::Windows::Forms::DockStyle::Top;
			this->originalImageLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Bold,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->originalImageLabel->Location = System::Drawing::Point(0, 0);
			this->originalImageLabel->Name = L"originalImageLabel";
			this->originalImageLabel->Size = System::Drawing::Size(374, 33);
			this->originalImageLabel->TabIndex = 0;
			this->originalImageLabel->Text = L"Original Image";
			this->originalImageLabel->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// manipulatedImagePanel
			// 
			this->manipulatedImagePanel->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(139)), static_cast<System::Int32>(static_cast<System::Byte>(159)),
				static_cast<System::Int32>(static_cast<System::Byte>(183)));
			this->manipulatedImagePanel->Controls->Add(this->manipulatedImageContainer);
			this->manipulatedImagePanel->Controls->Add(this->manipulatedImageLabel);
			this->manipulatedImagePanel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->manipulatedImagePanel->Location = System::Drawing::Point(383, 3);
			this->manipulatedImagePanel->Name = L"manipulatedImagePanel";
			this->manipulatedImagePanel->Size = System::Drawing::Size(374, 263);
			this->manipulatedImagePanel->TabIndex = 2;
			// 
			// manipulatedImageContainer
			//
			this->manipulatedImageContainer->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(139)), static_cast<System::Int32>(static_cast<System::Byte>(159)),
				static_cast<System::Int32>(static_cast<System::Byte>(183)));
			this->manipulatedImageContainer->AutoScroll = true;
			this->manipulatedImageContainer->Controls->Add(this->manipulatedImageBrowser);
			this->manipulatedImageContainer->Dock = System::Windows::Forms::DockStyle::Fill;
			this->manipulatedImageContainer->Location = System::Drawing::Point(0, 33);
			this->manipulatedImageContainer->Name = L"manipulatedImageContainer";
			this->manipulatedImageContainer->Size = System::Drawing::Size(374, 230);
			this->manipulatedImageContainer->TabIndex = 2;
			// 
			// manipulatedImageBrowser
			// 
			this->manipulatedImageBrowser->Dock = System::Windows::Forms::DockStyle::Fill;
			this->manipulatedImageBrowser->Location = System::Drawing::Point(3, 3);
			this->manipulatedImageBrowser->MinimumSize = System::Drawing::Size(20, 20);
			this->manipulatedImageBrowser->Name = L"manipulatedImageBrowser";
			this->manipulatedImageBrowser->Size = System::Drawing::Size(748, 364);
			this->manipulatedImageBrowser->TabIndex = 1;
			this->manipulatedImageBrowser->ScrollBarsEnabled = false;
			this->manipulatedImageBrowser->IsWebBrowserContextMenuEnabled = false;
			// 
			// manipulatedImageLabel
			// 
			this->manipulatedImageLabel->Dock = System::Windows::Forms::DockStyle::Top;
			this->manipulatedImageLabel->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15.75F, System::Drawing::FontStyle::Bold,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->manipulatedImageLabel->Location = System::Drawing::Point(0, 0);
			this->manipulatedImageLabel->Name = L"manipulatedImageLabel";
			this->manipulatedImageLabel->Size = System::Drawing::Size(374, 33);
			this->manipulatedImageLabel->TabIndex = 1;
			this->manipulatedImageLabel->Text = L"Manipulated Image";
			this->manipulatedImageLabel->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// Main
			// 
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Main::Main_FormClosing);
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1045, 752);
			this->Controls->Add(this->mainTableLayoutPanel);
			this->Controls->Add(this->topPanel);
			this->Controls->Add(this->leftPanel);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Margin = System::Windows::Forms::Padding(2, 4, 2, 4);
			this->Name = L"Main";
			this->Text = L"Main";
			this->topPanel->ResumeLayout(false);
			this->topPanel->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->maximizeButtonPictureBox))->EndInit();
			this->leftPanel->ResumeLayout(false);
			this->leftPanel->PerformLayout();
			this->optionsPanel->ResumeLayout(false);
			this->optionsPanel->PerformLayout();
			this->vectorOptionsPanel->ResumeLayout(false);
			this->vectorOptionsPanel->PerformLayout();
			this->vectorOptionsContainer->ResumeLayout(false);
			this->vectorOptionsContainer->PerformLayout();
			this->optimizeContoursPanel->ResumeLayout(false);
			this->optimizeContoursPanel->PerformLayout();
			this->optimizeContoursContainer->ResumeLayout(false);
			this->optimizeToleranceTableLayout->ResumeLayout(false);
			this->optimizeToleranceTableLayout->PerformLayout();
			this->optimizeContoursCheckBoxPanel->ResumeLayout(false);
			this->alphaMaxLayoutPanel->ResumeLayout(false);
			this->alphaMaxLayoutPanel->PerformLayout();
			this->vectorOptionsbuttonPanel->ResumeLayout(false);
			this->objectsPanel->ResumeLayout(false);
			this->objectsPanel->PerformLayout();
			this->objectsSettingsPanel->ResumeLayout(false);
			this->objectsSettingsPanel->PerformLayout();
			this->contourPanel->ResumeLayout(false);
			this->contourPanel->PerformLayout();
			this->turdSizeLayoutPanel->ResumeLayout(false);
			this->turdSizeLayoutPanel->PerformLayout();
			this->removeSmallContourPanel->ResumeLayout(false);
			this->removeSmallContourPanel->PerformLayout();
			this->numofObjectsPanel->ResumeLayout(false);
			this->numObjectsTableLayoutPanel->ResumeLayout(false);
			this->numObjectsTableLayoutPanel->PerformLayout();
			this->ROITableLayoutPanel->ResumeLayout(false);
			this->ROITableLayoutPanel->PerformLayout();
			this->removeSmallContourButtonPanel->ResumeLayout(false);
			this->objectsButtonPanel->ResumeLayout(false);
			this->imageManipulationPanel->ResumeLayout(false);
			this->imageManipulationPanel->PerformLayout();
			this->imageManipulationSettingPanel->ResumeLayout(false);
			this->imageManipulationSettingPanel->PerformLayout();
			this->blurringPanel->ResumeLayout(false);
			this->blurringPanel->PerformLayout();
			this->blurringIterationsContainer->ResumeLayout(false);
			this->blurringIterationsTableLayout->ResumeLayout(false);
			this->blurringIterationsTableLayout->PerformLayout();
			this->blurringCheckBoxButtonPanel->ResumeLayout(false);
			this->binaryThresholdSettingsPanel->ResumeLayout(false);
			this->binaryThresholdSettingsPanel->PerformLayout();
			this->binaryThresholdSettingsContainer->ResumeLayout(false);
			this->binaryThresholdTresholdPanel->ResumeLayout(false);
			this->thresholdLayoutPanel->ResumeLayout(false);
			this->thresholdLayoutPanel->PerformLayout();
			this->binaryThresholdButtonPanel->ResumeLayout(false);
			this->imageManipulationButtonPanel->ResumeLayout(false);
			this->ROIPanel->ResumeLayout(false);
			this->takePicturePanel->ResumeLayout(false);
			this->calibrateCameraPanel->ResumeLayout(false);
			this->mainTableLayoutPanel->ResumeLayout(false);
			this->contourImagePanel->ResumeLayout(false);
			this->contourTableLayout->ResumeLayout(false);
			this->originalImagePanel->ResumeLayout(false);
			this->originalImageContainer->ResumeLayout(false);
			//(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->originalImagePictureBox))->EndInit();
			this->manipulatedImagePanel->ResumeLayout(false);
			this->manipulatedImageContainer->ResumeLayout(false);
			this->Icon = gcnew System::Drawing::Icon("Icons\\app.ico");
			this->ResumeLayout(false);

		}
#pragma endregion
private: 
	System::Void takePictureButton_Click(System::Object^  sender, System::EventArgs^  e) {
		if (checkForCamera() == EDS_ERR_OK) {

			originalImage = getMatFromStream(debugging);
		}
		if (originalImage.empty()) {
			err = EDS_ERR_COMM_DISCONNECTED;
		}
		if (err != EDS_ERR_OK) {
			filepath = "IMG_0388.jpg";
			// Create a new saveFileDialog and set the properties
			System::Windows::Forms::OpenFileDialog^  openFile = gcnew System::Windows::Forms::OpenFileDialog();
			openFile->DefaultExt = "jpg";
			openFile->Filter = "Image Files(*.BMP;*.JPG;*)|*.BMP;*.JPG";
			//open savefiledialog, if the user clicks ok, we proceed and save the user choosen filepath.
			if (System::Windows::Forms::DialogResult::OK == openFile->ShowDialog()) {
				filepath = openFile->FileName;
			}
			msclr::interop::marshal_context context;
			string openFilePath = context.marshal_as<std::string>(filepath);
			ROI = cliext::vector<System::Drawing::Point>{};
			originalImage = cv::imread(openFilePath, CV_LOAD_IMAGE_COLOR);
		}
		originalImage.copyTo(UnCropped);
		if (!ROI.empty()) {
			vector<cv::Point> tempPoints;
			tempPoints = vector<cv::Point>{
				cv::Point(ROI[0].X, ROI[0].Y),
				cv::Point(ROI[1].X, ROI[1].Y) };
			FindROI(&originalImage, tempPoints);
		}
		originalImage.copyTo(image);


		Bitmap^ bmp = gcnew Bitmap(image.cols,
			image.rows, image.step1(),
			System::Drawing::Imaging::PixelFormat::Format24bppRgb,
			IntPtr(image.data));
		this->originalImageBrowser->DocumentText = gcnew System::String(makeImg(bmp).c_str());
		startUpdate();
	}
	System::Void ROI_Click(System::Object^ sender, System::EventArgs^ e) {
		if (!UnCropped.empty()) {
			vector<cv::Point> tempPoints;
			UnCropped.copyTo(originalImage);
			while (tempPoints.size() == 0) {
				tempPoints = drawOnImage(originalImage, "FindROI");
			}
			FindROI(&originalImage, tempPoints);
			ROI = cliext::vector<System::Drawing::Point>{};
			ROI.push_back(System::Drawing::Point(tempPoints[0].x, tempPoints[0].y));
			ROI.push_back(System::Drawing::Point(tempPoints[1].x, tempPoints[1].y));
			
			originalImage.copyTo(image);
			Bitmap^ bmp = gcnew Bitmap(originalImage.cols,
				originalImage.rows, originalImage.step1(),
				System::Drawing::Imaging::PixelFormat::Format24bppRgb,
				IntPtr(originalImage.data));
			this->originalImageBrowser->DocumentText = gcnew System::String(makeImg(bmp).c_str());
			startUpdate();
		}
	}
	System::Void ROIReset_Click(System::Object^ sender, System::EventArgs^ e) {
		if (!originalImage.empty()) {
			ROI = cliext::vector<System::Drawing::Point>{};
			UnCropped.copyTo(originalImage);
			UnCropped.copyTo(image);
			Bitmap^ bmp = gcnew Bitmap(originalImage.cols,
				originalImage.rows, originalImage.step1(),
				System::Drawing::Imaging::PixelFormat::Format24bppRgb,
				IntPtr(originalImage.data));
			this->originalImageBrowser->DocumentText = gcnew System::String(makeImg(bmp).c_str());
			startUpdate();
		}
	}	

	int reportToProgress(BackgroundWorker^ worker, int progress) {
		worker->ReportProgress(progress);
		return progress += 10;
	}


	/// <summary> The primary function of the program. Processes an image, uses potrace and returns the result. </summary>
	/// <param name="sender"> the background worker. Used to check for cancellationPrending</param>
	/// <param name="e"> RunWorkerCompletedEventArgs containing the results and flags pertaining the worker </param>
	/// <returns> UpdateResults. Contains the resulting image as a svg in html format and a bitmap. </returns>
	UpdateResults^ runUpdate(Mat image, BackgroundWorker^ worker, DoWorkEventArgs ^ e) {
		int progress = 0;
		UpdateResults^ updateResults = gcnew UpdateResults;

		progress = reportToProgress(worker, progress);

		if(this->blurring)
			Blurring(&image, this->blurringTrackBar->Value, worker, e);
		if (worker->CancellationPending)
		{
			worker->ReportProgress(1); // tell progress it process is canceled
			e->Cancel = true;
			return updateResults;
		}
		progress = reportToProgress(worker, progress);
		if (this->otsu)
		{
			Otsu(&image, 70, 255);
		}
		progress = reportToProgress(worker, progress);

		Binary(&image, this->binaryThresholdThresholdTrackbar->Value, binary_max);
		progress = reportToProgress(worker, progress);

		if (this->invert) {
			invertImage(&image);
		}
		progress = reportToProgress(worker, progress);
		if (this->enableRemoveSmallContourCustomCheckBox->IsChecked && this->numOfObjectsTextbox->Text != "" &&int::Parse(this->numOfObjectsTextbox->Text) > 0) {
			DeleteSmallContours(&image, int::Parse(numOfObjectsTextbox->Text));

		}
		//image = aproxPoly(image, 4);		 
		progress = reportToProgress(worker, progress);

		cvtColor(image, image, CV_GRAY2BGR);



		potrace_bitmap_t *bm = new potrace_bitmap_t;
		potrace_path_t *p = new potrace_path_t;
		potrace_state_t *st = new potrace_state_t;
		int n, *tag;


		Mat image_bmp;
		image.convertTo(image_bmp, CV_8UC3);
		imwrite("out.bmp", image_bmp);
		FILE *fin = new FILE;
		fin = fopen("out.bmp", "rb");
		bm_read(fin, 0.5, &bm);
		fclose(fin);
		remove("out.bmp");

		if (worker->CancellationPending)
		{
			worker->ReportProgress(1); // tell progress it process is canceled
			e->Cancel = true;
			return updateResults;
		}
		this->param->alphamax = (double)alphaMaxTrackBar->Value / (double)alphaMaxTrackBar->Multiplier;
		this->param->opttolerance = (double)toleranceTrackBar->Value / (double)toleranceTrackBar->Multiplier;
		this->param->turdsize = int::Parse(turdSizeTextBox->Text);
		this->param->opticurve = optimize;
		progress = reportToProgress(worker, progress);
		st = potrace_trace(param, bm);
		bm_free(bm);
		p = st->plist;
		progress = reportToProgress(worker, progress);




		updateResults->svghtml = gcnew System::String(makeSvg(p, image.cols, image.rows, worker, e).c_str());
		if (worker->CancellationPending)
		{
			worker->ReportProgress(1); // tell progress it process is canceled
			e->Cancel = true;
			return updateResults;
		}
		progress = reportToProgress(worker, progress);




		//this->contourBrowser->DocumentText = svghtml;
		progress = reportToProgress(worker, progress);

		if (image.cols % 4 != 0) {
			Scalar color = image.at<uchar>(cv::Point(image.cols - 1, image.rows - 1));
			copyMakeBorder(image, image, 0, 0, 0, 4 - (image.cols % 4), BORDER_CONSTANT, Scalar(color[0], color[0], color[0]));
		}


		Bitmap^ bmp = gcnew Bitmap(image.cols,
			image.rows, image.step1(),
			System::Drawing::Imaging::PixelFormat::Format24bppRgb,
			IntPtr(image.data));

		updateResults->bmp = gcnew Bitmap(bmp);
		potrace_state_free(st);
		progress = reportToProgress(worker, progress);
		return updateResults;
	}
	/// <summary> Uses the resulting image from runUpdate to create a DXF file. </summary>
	/// <returns> Void </returns>
	System::Void createDXF() {
		msclr::interop::marshal_context context;
		string dxf_name = context.marshal_as<std::string>(filepath);
		ofstream myfile;
		myfile.open(dxf_name.c_str()); 


		potrace_bitmap_t *bm = new potrace_bitmap_t;
		potrace_path_t *p = new potrace_path_t;
		potrace_state_t *st = new potrace_state_t;
		int n, *tag;

		
		updateResults->bmp->Save("out.bmp", System::Drawing::Imaging::ImageFormat::Bmp);

		FILE *fin = new FILE;
		fin = fopen("out.bmp", "rb");
		
		bm_read(fin, 0.5, &bm);
		fclose(fin);
		remove("out.bmp");


		st = potrace_trace(param, bm);
		bm_free(bm);
		p = st->plist;
		page_header(myfile, image.cols, image.rows);
		p = st->plist;
		while (p != NULL) {
			n = p->curve.n;
			dpoint_t *c, *c1;
			page_polyline(myfile);
			for (int i = 0;i < n;i++) {
				c = p->curve.c[i];
				c1 = p->curve.c[mod(i - 1, n)];
				dpoint_t test = scalePoint(c1[2], scale);
				switch (p->curve.tag[i]) {
				case POTRACE_CORNER:
					page_vertex(myfile, 0, scalePoint(c1[2], scale), 0);
					page_vertex(myfile, 0, scalePoint(c[1], scale), 0);
					break;
				case POTRACE_CURVETO:
					bezier(myfile, 0, scalePoint(c1[2], scale), scalePoint(c[0], scale), scalePoint(c[1], scale), scalePoint(c[2], scale));
					break;
				}
			}
			page_seqend(myfile);
			if (false) {
				p = p->sibling;
			}
			else {
				p = p->next;
			}
		
		}
		page_end(myfile);
		myfile.close();
	}
	System::Void binaryThresholdThresholdTrackbar_ValueChanged(System::Object^ sender, System::EventArgs^ e) {
		CustomTrackBar^ trackbar = safe_cast<CustomTrackBar^>(sender);
		binary_thres = trackbar->Value;
		startUpdate();
	}
	System::Void blurringTrackbar_ValueChanged(System::Object^ sender, System::EventArgs^ e) {
		CustomTrackBar^ trackbar = safe_cast<CustomTrackBar^>(sender);
		this->morph_iterations = trackbar->Value;
		startUpdate();
	}
	System::Void toleranceTrackbar_ValueChanged(System::Object^ sender, System::EventArgs^ e) {
		CustomTrackBar^ trackbar = safe_cast<CustomTrackBar^>(sender);
		this->param->opttolerance = (double)trackbar->Value / (double)trackbar->Multiplier;
		startUpdate();
	}
	System::Void alphamaxTrackbar_ValueChanged(System::Object^ sender, System::EventArgs^ e) {
		CustomTrackBar^ trackbar = safe_cast<CustomTrackBar^>(sender);
		this->param->alphamax = (double)trackbar->Value / (double)trackbar->Multiplier;
		startUpdate();
	}
	System::Void Trackbar_ValueChanging(System::Object^ sender, System::EventArgs^ e) {
		CustomTrackBar^ trackbar = safe_cast<CustomTrackBar^>(sender);
		trackbar->Parent->Controls[2]->Text = (trackbar->Value / (float)trackbar->Multiplier).ToString();
	}
	System::Void textBox_KeyDown(System::Object^ sender, System::Windows::Forms::KeyEventArgs^ e) {
		TextBox^ textbox = safe_cast<TextBox^>(sender);
		if (textbox->Parent->Controls->Count == 3) {
			CustomTrackBar^ trackbar = safe_cast<CustomTrackBar^>(textbox->Parent->Controls[1]);
			if (e->KeyCode == Keys::Up) {
				trackbar->Value += 1;
				startUpdate();
			}
			if (e->KeyCode == Keys::Down) {
				trackbar->Value -= 1;
				startUpdate();
			}
			if (e->KeyCode == Keys::Enter) {
				if (textbox->Text != "") {
					trackbar->Value = Math::Round(float::Parse(textbox->Text) * trackbar->Multiplier);
					e->Handled = true;
					e->SuppressKeyPress = true;
				};
				startUpdate();
			}
		}
		else {
			if (e->KeyCode == Keys::Up) {
				if (textbox->Text != "")
					textbox->Text = (int::Parse(textbox->Text) + 1).ToString();
				else
					textbox->Text = "1";
				startUpdate();
			}
			if (e->KeyCode == Keys::Down) {
				if (textbox->Text != "" && textbox->Text != "0")
					textbox->Text = (int::Parse(textbox->Text) - 1).ToString();
				startUpdate();
			}
			if (e->KeyCode == Keys::Enter) {
				e->Handled = true;
				e->SuppressKeyPress = true;
				startUpdate();
			}
		}
	}
	System::Void TextBox_ValueChanged(System::Object^ sender, System::EventArgs^ e) {
		TextBox^ textbox = safe_cast<TextBox^>(sender);
	}

	System::Void OnButtonCollapseClick(System::Object^  sender, System::EventArgs^  e) {
		Button^ upperButton = safe_cast<Button^>(sender);
		if (upperButton->Parent->Parent->Controls[0]->Visible) 
		{
			upperButton->Image->RotateFlip(RotateFlipType::Rotate270FlipNone);
		}
		else
		{
			upperButton->Image->RotateFlip(RotateFlipType::Rotate90FlipNone);
		
		}
		upperButton->Parent->Parent->Controls[0]->Visible = !upperButton->Parent->Parent->Controls[0]->Visible;
		upperButton->Refresh();

	}
	System::Void topPanel_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
		TogMove = 1;
		MValX = e->X;
		MValY = e->Y;
	}
	System::Void topPanel_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
		TogMove = 0;
	}
	System::Void topPanel_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
		if (TogMove == 1) {
			this->SetDesktopLocation(MousePosition.X - MValX - leftPanel->Width, MousePosition.Y - MValY);
		}
	}
	System::Void logoPanel_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
		if (TogMove == 1) {
			this->SetDesktopLocation(MousePosition.X - MValX, MousePosition.Y - MValY);
		}
	}

	System::Void label4_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	System::Void label_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
		Label^ currentLabel = safe_cast<Label^>(sender);
		currentLabel->ForeColor = System::Drawing::Color::White;
	}
	System::Void label_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
		Label^ currentLabel = safe_cast<Label^>(sender);
		currentLabel->ForeColor = System::Drawing::Color::Black;
	}
	System::Void minimizeLabel_Click(System::Object^  sender, System::EventArgs^  e) {
		this->WindowState = FormWindowState::Minimized;
	}

	/// <summary> Function called when exportButton is clicked </summary>
	/// <param name="sender"> exportButton as object </param>
	/// <param name="e"> EventArgs </param>
	/// <returns> Void </returns>
	System::Void exportButton_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!image.empty()) {
			// Create a new saveFileDialog and set the properties
			System::Windows::Forms::SaveFileDialog^  saveFileExport = gcnew System::Windows::Forms::SaveFileDialog();
			saveFileExport->DefaultExt = "dxf";
			saveFileExport->Filter = "AutoCAD DXF files (*.dxf) |*.dxf";
			//open savefiledialog, if the user clicks ok, we proceed and save the user choosen filepath.
			if (System::Windows::Forms::DialogResult::OK == saveFileExport->ShowDialog()) {
				filepath = gcnew System::String(saveFileExport->FileName);
				//if the backgroundworker is allready running, we set requestForDxf to true, which is checked when the worker has completed its current work.
				// The worker will then run createDXF
				// Disable exportButton and color it differently to show that the request has been recieved and is worked on.
				if (backgroundWorker1->IsBusy) {
					requestForDxf = true;
					exportButton->Enabled = false;
					exportButton->BackColor = System::Drawing::Color::Gray;
					return;
				}
				createDXF();
			}
		}
	}
	System::Void negativeFilterCheckboxButton_Click(System::Object^  sender, System::EventArgs^  e) {
		CustomCheckboxButton::CustomCheckboxButton^ currentCheckboxButton = safe_cast<CustomCheckboxButton::CustomCheckboxButton^>(sender);
		if (currentCheckboxButton->IsChecked) {
			currentCheckboxButton->IsChecked = !currentCheckboxButton->IsChecked;
			currentCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkUnchecked.png");
		}
		else {
			currentCheckboxButton->IsChecked = !currentCheckboxButton->IsChecked;
			currentCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkChecked.png");
		}

		this->invert = currentCheckboxButton->IsChecked;
		startUpdate();

	}
	System::Void removeSmallObjectsCheckboxButton_Click(System::Object^  sender, System::EventArgs^  e) {
		CustomCheckboxButton::CustomCheckboxButton^ currentCheckboxButton = safe_cast<CustomCheckboxButton::CustomCheckboxButton^>(sender);
		if (currentCheckboxButton->IsChecked) {
			this->removeSmallObjects = false;
			startUpdate();
		}
		else {
			this->removeSmallObjects = true;
			startUpdate();
		}
	}
	System::Void optimizeCheckboxButton_Click(System::Object^  sender, System::EventArgs^  e) {
		CustomCheckboxButton::CustomCheckboxButton^ currentCheckboxButton = safe_cast<CustomCheckboxButton::CustomCheckboxButton^>(sender);
		this->optimize = !currentCheckboxButton->IsChecked;
		startUpdate();
		
		
	}
	System::Void CheckboxButton_Click(System::Object^  sender, System::EventArgs^  e) {
		CustomCheckboxButton::CustomCheckboxButton^ currentCheckboxButton = safe_cast<CustomCheckboxButton::CustomCheckboxButton^>(sender);
		if (currentCheckboxButton->IsChecked) {
			currentCheckboxButton->IsChecked = !currentCheckboxButton->IsChecked;
			currentCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkUnchecked.png");
		}
		else {
			currentCheckboxButton->IsChecked = !currentCheckboxButton->IsChecked;
			currentCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkChecked.png");
		}
	}
	System::Void maximizeButtonPictureBox_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
			this->maximizeButtonPictureBox->Image = Image::FromFile("Icons\\MaximizeInverted.png");
	}
	System::Void maximizeButtonPictureBox_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
		this->maximizeButtonPictureBox->Image = Image::FromFile("Icons\\Maximize.png");
	}
	System::Void maximizeButtonPictureBox_Click(System::Object^  sender, System::EventArgs^  e) {
		if (this->WindowState == FormWindowState::Maximized) {
			this->WindowState = FormWindowState::Normal;
		}
		else {
			this->WindowState = FormWindowState::Maximized;
		}
	}
	/// <summary> Starts the background worker, which is on another thread. The worker runs the runUpdate function and puts the result in e->results </summary>
	/// <param name="sender"> the background worker </param>
	/// <param name="e"> DoWorkEventArgs containing the results and flags pertaining the worker </param>
	/// <returns> Void </returns>
	System::Void backgroundWorker1_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {
		BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
		e->Result = runUpdate(originalImage.clone(), worker, e);
	}

	/// <summary> Called when background worker either completes a process or is canceled. If the worker was canceled we run the process again, otherwise we update images and results </summary>
	/// <param name="sender"> the background worker </param>
	/// <param name="e"> RunWorkerCompletedEventArgs containing the results and flags pertaining the worker </param>
	/// <returns> Void </returns>
	System::Void backgroundWorker1_RunWorkerCompleted(Object^ sender, RunWorkerCompletedEventArgs^ e) {
		BackgroundWorker^ worker = dynamic_cast<BackgroundWorker^>(sender);
		if (e->Cancelled) 
			worker->RunWorkerAsync();
		else {
			UpdateResults^ results = safe_cast<UpdateResults^>(e->Result);
			this->manipulatedImageBrowser->DocumentText = gcnew System::String(makeImg((Bitmap^)results->bmp).c_str());
			this->contourBrowser->DocumentText = results->svghtml;
			updateResults->bmp = gcnew Bitmap(results->bmp);
			
			// if we got a request for creating a dxf through exportbutton click, we create the dxf now that we are finished. 
			//Export button is reenabled and recolored.
			if (requestForDxf) {
				requestForDxf = false;
				createDXF();
				exportButton->Enabled = true;
				exportButton->BackColor= System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(73)), static_cast<System::Int32>(static_cast<System::Byte>(97)),
					static_cast<System::Int32>(static_cast<System::Byte>(125)));
			}
			//return cursor to default
			this->Cursor = Cursors::Default; 
		}
	}

	/// <summary> Used to report back the progress of the process using a progress bar. A switch case is used to write to a label. </summary>
	/// <param name="e"> ProgressChangedEventArgs which holds the value for the progress bar </param>
	/// <returns> Void </returns>
	System::Void backgroundWorker1_ProgressChanged(Object^ /*sender*/, ProgressChangedEventArgs^ e)
	{
		this->progressBar1->Value = e->ProgressPercentage;
		switch (e->ProgressPercentage)
		{
		case 0:
			progressBar1->Visible = true;
			progressBarLabel->Visible = true;
			progressBarLabel->Text = "Blurring";
			break;
		case 10:
			progressBarLabel->Text = "Otsu";
			break;
		case 20:
			progressBarLabel->Text = "Binary";
			break;
		case 30:
			progressBarLabel->Text = "Removing objects";
			break;
		case 40:
			progressBarLabel->Text = "Inverting Image colors";
			break;
		case 50:
			progressBarLabel->Text = "Preparing for vectorization";
			break;
		case 60:
			progressBarLabel->Text = "Vectorizing";
			break;
		case 70:
			progressBarLabel->Text = "Preparing to show vectorized image";
			break;
		case 80:
			progressBarLabel->Text = "Vectorizing image";
			break;
		case 90:
			progressBarLabel->Text = "Preparing to show manipulated image";
			break;
		case 100:
			progressBarLabel->Text = "Process Completed";
			progressBar1->Visible = false;
			progressBarLabel->Visible = false;
			break;
		default:
			progressBarLabel->Text = "Process canceled, starting over with new parameters";
			break;
		}
	}

	/// <summary> Starts the background worker, which is on another thread. If the worker is already running it is set to cancel, otherwise it starts the worker </summary>
	/// <returns> Void </returns>
	System::Void startUpdate() {
		if (!image.empty()) {
			if (backgroundWorker1->IsBusy) {
				backgroundWorker1->CancelAsync();
			}
			else
			{
				this->Cursor = Cursors::AppStarting; // to better show the program is running, we change to cursor to Appstarting.
				backgroundWorker1->RunWorkerAsync();
			}
				
		}
	}
private: System::Void enableRemoveSmallContourCustomCheckBox_Click(System::Object^  sender, System::EventArgs^  e) {
	CustomCheckboxButton::CustomCheckboxButton^ currentCheckboxButton = safe_cast<CustomCheckboxButton::CustomCheckboxButton^>(sender);
	if (currentCheckboxButton->IsChecked) {
		currentCheckboxButton->IsChecked = !currentCheckboxButton->IsChecked;
		currentCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkUnchecked.png");
	}
	else {
		currentCheckboxButton->IsChecked = !currentCheckboxButton->IsChecked;
		currentCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkChecked.png");
	}
	this->numofObjectsPanel->Visible = currentCheckboxButton->IsChecked;
}
private: System::Void blurringCheckBoxButton_Click(System::Object^  sender, System::EventArgs^  e) {
	CustomCheckboxButton::CustomCheckboxButton^ currentCheckboxButton = safe_cast<CustomCheckboxButton::CustomCheckboxButton^>(sender);
	if (currentCheckboxButton->IsChecked) {
		currentCheckboxButton->IsChecked = !currentCheckboxButton->IsChecked;
		currentCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkUnchecked.png");
	}
	else {
		currentCheckboxButton->IsChecked = !currentCheckboxButton->IsChecked;
		currentCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkChecked.png");
	}
	this->blurring = currentCheckboxButton->IsChecked;
	this->blurringIterationsContainer->Visible = currentCheckboxButton->IsChecked;
}


private: System::Void calibrateCameraButton_Click(System::Object^  sender, System::EventArgs^  e) {
	if (!image.empty()) {
		MessageBox::Show("Please select region of interest around the A4 paper.");
		Mat calibrateImage = image.clone();
		vector<cv::Point> tempPoints = vector<cv::Point>{};

		while (tempPoints.size() == 0) {
			tempPoints = drawOnImage(calibrateImage, "Calibrate Camera");
		}
		FindROI(&calibrateImage, tempPoints);
		scale = FindPaperLongSide(calibrateImage, (double)binaryThresholdThresholdTrackbar->Value);
	}
}

private: System::Void Main_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
	if (ROI.empty()) {
		ROI = cliext::vector<System::Drawing::Point>{};
		ROI.push_back(System::Drawing::Point(0, 0));
		ROI.push_back(System::Drawing::Point(0, 0));
	}
	Write_Settings_To_XML(int::Parse(this->turdSizeTextBox->Text->ToString()),
							this->alphaMaxTrackBar->Value,
							this->toleranceTrackBar->Value,
							this->blurringCheckBoxButton->IsChecked,
							this->blurringTrackBar->Value,
							this->binaryThresholdThresholdTrackbar->Value,
							scale,
							optimize,
							this->negativeFilterCheckboxButton->IsChecked,
							ROI[0].X, ROI[0].Y,
							ROI[1].X, ROI[1].Y);

}



private: void Read_Setting_From_XML() {
	if (XML_file_exists()) {
		FileStorage fs("Settings.xml", FileStorage::READ);
		int roi1x, roi1y, roi2x, roi2y;
		scale = (double)fs["scale"];
		binary_thres = (double)fs["binary_thres"];
		turdSize = (int)fs["turdSize"];
		alphamax = (int)fs["alphaMax"];
		tolerance = (int)fs["tolerance"];
		blurring = (int)fs["blurring"];
		morph_iterations = (int)fs["morph_iterations"];
		optimize = (int)fs["optimize"];
		invert = (int)fs["invert"];
		ROI = cliext::vector<System::Drawing::Point>{};
		
		roi1x = (int)fs["roi1x"];
		roi1y = (int)fs["roi1y"];
		roi2x = (int)fs["roi2x"];
		roi2y = (int)fs["roi2y"];
		if (!(roi1x == 0 && roi1y == 0 && roi2x == 0 && roi2y == 0)) {
			ROI.push_back(System::Drawing::Point(roi1x, roi1y));
			ROI.push_back(System::Drawing::Point(roi2x, roi2y));
		}

		
			
	}

}

private: System::Void optimizeContoursCheckboxButton_Click(System::Object^  sender, System::EventArgs^  e) {
	CustomCheckboxButton::CustomCheckboxButton^ currentCheckboxButton = safe_cast<CustomCheckboxButton::CustomCheckboxButton^>(sender);
	if (currentCheckboxButton->IsChecked) {
		currentCheckboxButton->IsChecked = !currentCheckboxButton->IsChecked;
		currentCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkUnchecked.png");
	}
	else {
		currentCheckboxButton->IsChecked = !currentCheckboxButton->IsChecked;
		currentCheckboxButton->Image = Image::FromFile("Icons\\CheckMarkChecked.png");
	}
	this->optimizeContoursContainer->Visible = currentCheckboxButton->IsChecked;

	this->optimize = currentCheckboxButton->IsChecked;
	this->param->opticurve = optimize;
	startUpdate();
}
};
}
