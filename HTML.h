#ifndef HTML_H
#define HTML_H
#include <string>
#include <windows.h>

/// <summary> Creates the HTML string for viewing svg or img </summary>
/// <param name="html"> The input html for the svg or img </param> 
/// <returns> string containing html for viewing the svg or img </returns>
std::string createHTML(std::string html);
/// <summary> Returns a string containing the path to the working directory </summary>
/// <returns> string containing path to working directory </returns>
std::string workingdir();

#endif // HTML_H